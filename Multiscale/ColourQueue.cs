﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;

namespace Multiscale
{

    class ColorQueue
    {
        private Random random = new Random();
        private List<Color> colors;
        public int CurrentColor { get; set; }
        public int CurrentColorPool { get; set; }

        public ColorQueue(int colorPool)
        {
            colors = new List<Color>(colorPool);
            CurrentColorPool = colorPool;
            CurrentColor = 0;
            for(var i =0; i < colorPool; ++i)
            {
                var RGB = new Tuple<int, int, int>(0, random.Next(0, 256), random.Next(0, 256));
                colors.Add(Color.FromArgb(RGB.Item1, RGB.Item2, RGB.Item3));
            }
        }

        public ColorQueue(int colorPool, Func<Color> colorGenerator)
        {
            colors = new List<Color>(colorPool);
            CurrentColorPool = colorPool;
            CurrentColor = 0;
            for (var i = 0; i < colorPool; ++i)
            {
                var color = colorGenerator();
                colors.Add(color);
            }
        }

        public ColorQueue(List<Color> colors)
        {
            this.colors = colors;
            CurrentColorPool = colors.Count;
            CurrentColor = 0;
        }

        public Color GetColorAndForward()
        {
            CurrentColor = (CurrentColor + 1) % CurrentColorPool;
            var color = colors[CurrentColor];
            return color;   
        }

        public Color GetCurrentColor()
        {
            return colors[CurrentColor];
        }
        
        public void SetColoursPool(int coloursPool)
        {
            if(coloursPool > CurrentColorPool)
                for(var i =0; i < coloursPool - CurrentColorPool; ++i)
                {
                    var RGB = new Tuple<int, int, int>(0, random.Next(0, 256), random.Next(0, 256));
                    colors.Add(Color.FromArgb(RGB.Item1, RGB.Item2, RGB.Item3));
                }
            else if(coloursPool < CurrentColorPool)
                colors = colors.Take(coloursPool).ToList();

            CurrentColorPool = coloursPool;
            CurrentColor = 0;
        }

        public void AddNewColor()
        {
            SetColoursPool(CurrentColorPool + 1);
        }

        public int GetLatestCrystalAsNumber()
        {
            return CurrentColorPool;
        }

        public Color GetColourFromTable(int color)
        {
            return colors[color];
        }

        public List<Color> GetColours()
        {
            return colors;
        }
    }
}
