﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace Multiscale.Bounds
{
    interface IBounds
    {
        IEnumerable<Point> GetPointsInside(Point centre, int radius, Tuple<Point, Point> corners);
    }
}
