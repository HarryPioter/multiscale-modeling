﻿using Multiscale.Bounds;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace Multiscale
{
    public class CircleBounds : IBounds
    {
        public IEnumerable<Point> GetPointsInside(Point centre, int radius, Tuple<Point, Point> corners)
        {
            if (radius == 0)
            {
                yield return centre;
            }

            Point upperLeftCorner = corners.Item1,
                lowerRightCorner = corners.Item2;

            int xStart = Math.Max(upperLeftCorner.X, centre.X - radius),
                xEnd = Math.Min(lowerRightCorner.X, centre.X + radius),
                yStart = Math.Max(upperLeftCorner.Y, centre.Y - radius),
                yEnd = Math.Min(lowerRightCorner.Y, centre.Y + radius);

            var radiusSquared = radius * radius;
            for (var y = yStart; y <= yEnd; ++y)
                for (var x = xStart; x <= xEnd; ++x)
                {
                    int xLen = x - centre.X,
                        yLen = y - centre.Y;
                    var dotProd = xLen * xLen + yLen * yLen;
                    if (dotProd <= radiusSquared)
                        yield return new Point(x, y);
                }
        }
    }
}
