﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace Multiscale.Bounds
{
    public class SquareBounds : IBounds
    {
        public IEnumerable<Point> GetPointsInside(Point centre, int radius, Tuple<Point, Point> corners)
        {
            if (radius == 0)
            {
                yield return centre;
            }

            Point upperLeftCorner = corners.Item1,
                lowerRightCorner = corners.Item2;

            int xStart = Math.Max(upperLeftCorner.X, centre.X - radius),
                xEnd = Math.Min(lowerRightCorner.X, centre.X + radius),
                yStart = Math.Max(upperLeftCorner.Y, centre.Y - radius),
                yEnd = Math.Min(lowerRightCorner.Y, centre.Y + radius);

            for (var y = yStart; y <= yEnd; ++y)
                for (var x = xStart; x <= xEnd; ++x)
                        yield return new Point(x, y);
        }
    }
}
