﻿using System.Collections.Generic;

namespace Multiscale
{
    class Grid
    {
        public short[,] board;
        public short[,] energy;
        public bool[,] IsRecrystalized;
        public Grid(int xDimension, int yDimension)
        {
            board = new short[xDimension + 2, yDimension + 2];
            energy = new short[xDimension + 2, yDimension + 2];
            IsRecrystalized = new bool[xDimension + 2, yDimension + 2];
        }

        public void SetGrains(short val)
        {
            for (int i = 0; i < board.GetLength(0); ++i)
                for (int j = 0; j < board.GetLength(1); ++j)
                    board[i, j] = val;
        }

        public void SetRecrystalizaitonFlags(bool val)
        {
            for (int i = 0; i < IsRecrystalized.GetLength(0); ++i)
                for (int j = 0; j < IsRecrystalized.GetLength(1); ++j)
                    IsRecrystalized[i, j] = val;
        }

        public void SetEnergies(short val)
        {
            for (int i = 0; i < energy.GetLength(0); ++i)
                for (int j = 0; j < energy.GetLength(1); ++j)
                    energy[i, j] = val;

        }
    }
}
