﻿namespace Multiscale
{
    class Offset
    {
        public static readonly Offset[] offsets =
        {
            new Offset(-1,-1),new Offset(0,-1),new Offset(1,-1),
            new Offset(-1,0),new Offset(1,0),
            new Offset(-1,1),new Offset(0,1),new Offset(1,1)
        };

        public int X { get; set; }
        public int Y { get; set; }
        public Offset(int x, int y)
        {
            X = x;
            Y = y;
        }
    }


}
