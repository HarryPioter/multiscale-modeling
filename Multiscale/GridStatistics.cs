﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Multiscale
{
    class GridStatistics
    {
        IEnumerable<Tuple<int, int>> CountGrains(Grid grid, ColorQueue queue)
        {
            return Enumerable.Range(1, queue.CurrentColorPool).Select(
                grainId => {
                    var grainCount = grid.board.Cast<short>().Count(id => id == grainId);
                    return new Tuple<int, int>(grainId, grainCount);
                }
            );
        }

        public IEnumerable<GrainItem> GenerateListItems(Grid grid, ColorQueue queue)
        {
            var counters = CountGrains(grid, queue);
            return counters.Select(counter => new GrainItem()
            {
                GrainId = (short)counter.Item1,
                Color = queue.GetColourFromTable(counter.Item1 - 1),
                Text = $"Id: {counter.Item1}, Count: {counter.Item2}",
                Count = counter.Item2
            });
        }
    }
}
