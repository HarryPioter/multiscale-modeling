﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using Multiscale.Bounds;

namespace Multiscale
{
    public partial class Form1 : Form
    {
        private Grid Grid;
        private ClickGrid ClickGrid;
        private ColorQueue Queue;
        private Evolver Evolver;
        private bool ContinousGrainRandomization;
        private int iterationLimit = 0;
        private int currentIteration = 0;
        private Random random = new Random();
        private List<short> ignoredGrainIds = new List<short>();
        private Bitmap simulationBackground;
        public Form1()
        {
            InitializeComponent();
            Queue = new ColorQueue(2);
            var y = 100;
            var x = 2* y;
            clearPictureBox();
            ContinousGrainRandomization = false;
            Grid = new Grid(x, y);
            ClickGrid = new ClickGrid(pictureBox.Size, new Size(x, y));
            
            var initialBmp = new Bitmap(colourBox.Size.Width, colourBox.Size.Height);
            using (Graphics g = Graphics.FromImage(initialBmp))
            {
                g.Clear(Queue.GetColorAndForward());
            }
            colourBox.Image = initialBmp;
            colourBox.Refresh();

            var initialBmp2 = new Bitmap(energyBox.Size.Width, energyBox.Size.Height);
            using (Graphics g = Graphics.FromImage(initialBmp2))
            {
                g.Clear(Color.White);
            }
            energyBox.Image = initialBmp2;
            energyBox.Refresh();

            Evolver = new Evolver(Grid, Queue.CurrentColorPool);
            var neighbourhood = currentNeighbourhood();
            Evolver.Neighbourhood = neighbourhood;

            numericUpDown1.Minimum = 2;
            numericUpDown1.Value = 3;

            grainRatioNumeric.Minimum = 0.01M;
            grainRatioNumeric.Maximum = 0.9999M;

            evenNumeric.Minimum = 1;
            evenNumeric.Maximum = x * y;

            radiusNumeric.Minimum = 1;
            radiusNumeric.Maximum = Math.Max(x, y);

            numericUpDown2.Minimum = 0;
            numericUpDown2.Maximum = (decimal)(Grid.board.GetLength(0) * Grid.board.GetLength(1) * 0.05f);
            tabControl1.SelectedIndexChanged += TabControl1_SelectedIndexChanged;

            grainsListBox.DisplayMember = "Text";
            grainsListBox.ValueMember = "GrainId";
            grainsListBox.DrawItem += GrainsListBox_DrawItem;
        }

        private void GrainsListBox_DrawItem(object sender, DrawItemEventArgs e)
        {
            e.DrawBackground();
            Graphics g = e.Graphics;

            var item = grainsListBox.Items[e.Index] as GrainItem;
            e.Graphics.FillRectangle(new SolidBrush(item.Color), e.Bounds);
            e.Graphics.DrawString(item.Text, e.Font, Brushes.Black, e.Bounds, StringFormat.GenericDefault);

            e.DrawFocusRectangle();
        }

        private void TabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl1.SelectedTab == thridTabPage)
            {
                grainsListBox.Items.Clear();
                var items = new GridStatistics().GenerateListItems(Grid, Queue)
                    .Where(item => item.Count > 0)
                    .OrderByDescending(item => item.Count)
                    .ToList();
                foreach (var item in items)
                {
                    grainsListBox.Items.Add(item);
                }
            }
        }

        void Tabs_Selected(object sender, TabControlEventArgs e)
        {
            if (e.TabPage == thridTabPage)
            {
                var items = new GridStatistics().GenerateListItems(Grid, Queue);
                foreach (var item in items)
                {
                    grainsListBox.Items.Add(item);
                }
            }
        }

        private void DrawBoard()
        {
            var bufferImage = new Bitmap(pictureBox.Image);
            using (Graphics g = Graphics.FromImage(bufferImage))
            {
                var pixelsPerCellDx = pictureBox.Size.Width / (float)(Grid.board.GetLength(0) - 2);
                var pixelsPerCellDy = pictureBox.Size.Height / (float)(Grid.board.GetLength(1) - 2);

                for (int i = 1; i < Grid.board.GetLength(0) - 1; ++i)
                    for (int j = 1; j < Grid.board.GetLength(1) - 1; ++j)
                    {

                        if (Grid.board[i, j] != 0)
                        {
                            var colour = Color.Black;
                            if (Grid.board[i, j] > 0)
                                colour = Queue.GetColourFromTable(Grid.board[i, j] - 1);

                            g.FillRectangle(new SolidBrush(colour),
                                            (i - 1) * pixelsPerCellDx, (j - 1) * pixelsPerCellDy,
                                            pixelsPerCellDx, pixelsPerCellDy);
                        }

                    }
            }
            pictureBox.Image = bufferImage;
            pictureBox.Refresh();
        }

        private void DrawEnergy()
        {
            //if (homogenRadio.Checked)
            //    DrawHomogenousEnergy();
            //else
                DrawHeterogenousEnergy();
        }

        private void DrawHeterogenousEnergy()
        {
            var bufferImage = new Bitmap(energyBox.Image);
            using (Graphics g = Graphics.FromImage(bufferImage))
            {
                var highEnergy = (short)highEnergyNumericUpDown3.Value;
                var lowEnergy = (short)lowEnergyNumericUpDown.Value;
                for (int i = 1; i < Grid.energy.GetLength(0) - 1; ++i)
                    for (int j = 1; j < Grid.energy.GetLength(1) - 1; ++j)
                    {
                        var energy = Grid.energy[i, j];
                        Color color;
                        if (energy == lowEnergy)
                            color = Color.DeepSkyBlue;
                        else if (energy == highEnergy)
                            color = Color.LawnGreen;
                        else
                            color = Color.Blue;
                        
                        g.FillRectangle(new SolidBrush(color),
                                            (i - 1), (j - 1),
                                            1, 1);
                    }
            }
            energyBox.Image = bufferImage;
            energyBox.Refresh();
        }

        private void DrawHomogenousEnergy()
        {
            var initialBmp = new Bitmap(energyBox.Size.Width, energyBox.Size.Height);
            using (Graphics g = Graphics.FromImage(initialBmp))
            {
                g.Clear(Color.DeepSkyBlue);
            }
            energyBox.Image = initialBmp;
            energyBox.Refresh();
        }

        private void DistributeEnergy(Grid grid, Neighbourhood nbhood)
        {
            if (homogenRadio.Checked)
                DistributeEnergyHomogenous(grid);
            else
                DistributeEnergyHeterogenous(grid, Evolver.Neighbourhood);
        }

        private void DistributeEnergyHeterogenous(Grid grid, Neighbourhood nbhood)
        {
            var moore = new MooreNeighbourhood(nbhood.Idx);
            var highEnergy = (short)highEnergyNumericUpDown3.Value;
            var lowEnergy = (short)lowEnergyNumericUpDown.Value;

            for (int i = 1; i < grid.energy.GetLength(0) - 1; ++i)
                for (int j = 1; j < grid.energy.GetLength(1) - 1; ++j)
                {
                    var isOnEdge = moore.IsOneTheEdge(grid.board, new Point(i, j));
                    if (isOnEdge)
                        grid.energy[i, j] = highEnergy;
                    else
                        grid.energy[i, j] = lowEnergy;
                        
                }
        }

        private void DistributeEnergyHomogenous(Grid grid)
        {
            var lowEnergy = (short)lowEnergyNumericUpDown.Value;

            for (int i = 1; i < grid.energy.GetLength(0) - 1; ++i)
                for (int j = 1; j < grid.energy.GetLength(1) - 1; ++j)
                    grid.energy[i, j] = lowEnergy;
        }

        private Neighbourhood currentNeighbourhood()
        {
            var idx = currentIndexTranslator();
            if (radioButton1.Checked)
                return new VonNeumannNeighbourhood(idx);
            else
                return new MooreNeighbourhood(idx, (int)mooreNumeric.Value);
        }

        private IIndexTranslator currentIndexTranslator()
        {
            if (radioButton7.Checked)
                return new NonperiodicIndexTranslator();
            else
                return new PeriodicIndexTranslator(Grid.board.GetLength(0), Grid.board.GetLength(1));
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            Queue.SetColoursPool((int)numericUpDown1.Value);
            Queue.SetColoursPool((int)numericUpDown1.Value);
            
            using (Graphics g = Graphics.FromImage(colourBox.Image))
            {
                g.Clear(Queue.GetColorAndForward());
            }
            colourBox.Refresh();
        }

        private void colourBox_Click(object sender, EventArgs e)
        {
            using(Graphics g = Graphics.FromImage(colourBox.Image))
            {
                g.Clear(Queue.GetColorAndForward());
            }
            colourBox.Refresh();
        }

        private void pictureBox_Click(object sender, EventArgs e)
        {
            stopButton.Enabled = false;
            stopButton.PerformClick();
            var mouseClick = (MouseEventArgs)e;
            var originPoint = ClickGrid.getCellIndices(mouseClick.X, mouseClick.Y);
            if (ModifierKeys == Keys.Shift)
            {
                var moorNbhood = new MooreNeighbourhood(Evolver.Neighbourhood.Idx);
                var isOnEdge = moorNbhood.IsOneTheEdge(Grid.board, new Point(originPoint.Item1, originPoint.Item2));
                if (isOnEdge)
                {
                    const int inclusionId = -1;
                    IEnumerable<Point> points;
                    IBounds checker = null;
                    if (radialInclusionRadioButton.Checked)
                        checker = new CircleBounds();
                    else
                        checker = new SquareBounds();

                    points = checker.GetPointsInside(
                        new Point(originPoint.Item1 + 1, originPoint.Item2 + 1),
                        (int)inclusionsNumeric.Value,
                        new Tuple<Point, Point>(new Point(1, 1), new Point(202, 102))
                        );
                    foreach (var point in points)
                    {
                        Grid.board[point.X, point.Y] = inclusionId;
                    }
                }
            }
            else
            {
                Grid.board[originPoint.Item1 + 1, originPoint.Item2 + 1] = (short)(Queue.CurrentColor + 1);
                //Grid.energy[originPoint.Item1 + 1, originPoint.Item2 + 1] = 0;
                //Grid.IsRecrystalized[originPoint.Item1 + 1, originPoint.Item2 + 1] = true;
            }
            Console.WriteLine("x " + originPoint.Item1 + " y " + originPoint.Item2);
            DrawBoard();
            startButton.Enabled = true;
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            var watch = new Stopwatch();
            watch.Start();
            var isInProgress = Evolver.Evolve(ignoredGrainIds);
            watch.Stop();
            Console.WriteLine(watch.Elapsed.Milliseconds);
            if (isInProgress)
            {
                if (ContinousGrainRandomization)
                    addNewGrains((int)numericUpDown2.Value);
                DrawBoard();
            }
            else {
                stopButton.PerformClick();
                recrystalizeButton.Enabled = true;
            }
        }

        private void startButton_Click(object sender, EventArgs e)
        {
            stopButton.Enabled = true;
            startButton.Enabled = false;
            timer.Start();
        }

        private void stopButton_Click(object sender, EventArgs e)
        {
            startButton.Enabled = true;
            stopButton.Enabled = false;
            timer.Stop();
            DrawBoard();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Evolver.Evolve(ignoredGrainIds);
            if (ContinousGrainRandomization)
                addNewGrains((int)numericUpDown2.Value);
            DrawBoard();
        }

        private void radioButton7_CheckedChanged(object sender, EventArgs e)
        {
            stopButton.PerformClick();
            Evolver.Neighbourhood = currentNeighbourhood();
        }

        private void radioButton8_CheckedChanged(object sender, EventArgs e)
        {
            stopButton.PerformClick();
            Evolver.Neighbourhood = currentNeighbourhood();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            stopButton.PerformClick();
            Evolver.Neighbourhood = new VonNeumannNeighbourhood(Evolver.Neighbourhood.Idx);
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            stopButton.PerformClick();
            Evolver.Neighbourhood = new MooreNeighbourhood(Evolver.Neighbourhood.Idx);
        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            stopButton.Enabled = false;
            stopButton.PerformClick();
            startButton.Enabled = true;
            for (int i = 1; i < Grid.board.GetLength(0) - 1; ++i)
                for (int j = 1; j < Grid.board.GetLength(1) - 1; ++j)
                    Grid.board[i, j] = 0;
            clearPictureBox();

        }

        private void clearPictureBox()
        {
            var initialBitmap = new Bitmap(pictureBox.Size.Width, pictureBox.Size.Height);
            using (Graphics g = Graphics.FromImage(initialBitmap))
            {
                g.Clear(Color.White);
            }
            pictureBox.Image = initialBitmap;
            pictureBox.Refresh();
        }

        private void randomButton_Click(object sender, EventArgs e)
        {
            stopButton.PerformClick();
            clearPictureBox();
            RandomizeDomain(mcRandomizeCheckbox.Checked, false);
            DrawBoard();
        }

        private void RandomizeDomain(bool isMcMode, bool ignoreListedGrains)
        {
            if (isMcMode)
                RandomizeAllAvailabelCells(ignoreListedGrains);
            else
                RandomizeSomeAvailableCells((double)grainRatioNumeric.Value, ignoreListedGrains);
        }

        private void RandomizeAllAvailabelCells(bool ignoreListedGrains)
        {
            for (int i = 1; i < Grid.board.GetLength(0) - 1; ++i)
                for (int j = 1; j < Grid.board.GetLength(1) - 1; ++j)
                    if (Grid.board[i, j] == 0)
                    {
                        short val = (short)random.Next(1, Queue.CurrentColorPool + 1);
                        while (ignoreListedGrains && ignoredGrainIds.Contains(val))
                            val = (short)random.Next(1, Queue.CurrentColorPool + 1);

                        Grid.board[i, j] = val;
                    }
        }

        private void RandomizeSomeAvailableCells(double ratio, bool ignoreListedGrains)
        {
            if (ignoreListedGrains)
            {
                for (int i = 1; i < Grid.board.GetLength(0) - 1; ++i)
                    for (int j = 1; j < Grid.board.GetLength(1) - 1; ++j)
                    {
                        var current = Grid.board[i, j];
                        if (current == 0 && random.NextDouble() > ratio)
                        {
                            short val = (short)random.Next(1, Queue.CurrentColorPool + 1);
                            while(ignoredGrainIds.Contains(val))
                                val = (short)random.Next(1, Queue.CurrentColorPool + 1);
                            Grid.board[i, j] = val;
                        }
                    } 
            }
            else
            {
                for (int i = 1; i < Grid.board.GetLength(0) - 1; ++i)
                    for (int j = 1; j < Grid.board.GetLength(1) - 1; ++j)
                        if (random.NextDouble() < ratio)
                            Grid.board[i, j] = 0;
                        else
                        {
                            short val = (short)random.Next(1, Queue.CurrentColorPool + 1);
                            Grid.board[i, j] = val;
                        }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            stopButton.PerformClick();
            clearButton.PerformClick();
            
            int boardWidth = Grid.board.GetLength(0),
                boardHeigth = Grid.board.GetLength(1);
            int cellsDemand = (int)evenNumeric.Value,
                cellsCounter = 0,
                failureCounter = 0,
                failureLimit = 200;
            Random random = new Random(),
                randomGrain = new Random();

            while(cellsCounter < cellsDemand && failureCounter < failureLimit)
            {
                var x = random.Next(1, boardWidth - 1);
                var y = random.Next(1, boardHeigth - 1);
                if (Grid.board[x, y] == 0)
                {
                    Grid.board[x, y] = (byte)randomGrain.Next(1, Queue.CurrentColorPool + 1);
                    ++cellsCounter;
                }
                else
                    ++failureCounter;
            }

            DrawBoard();
        }

        private void radiusButton_Click(object sender, EventArgs e)
        {
            stopButton.PerformClick();
            clearButton.PerformClick();

            int boardWidth = Grid.board.GetLength(0),
                boardHeigth = Grid.board.GetLength(1);
            int cellsDemand = (int)evenNumeric.Value,
                cellsCounter = 0,
                failureCounter = 0,
                failureLimit = 400;
            var radius = (float)radiusNumeric.Value;
            var cellList = new List<Tuple<int, int>>(cellsDemand);
            var random = new Random();

            while(cellsCounter < cellsDemand && failureCounter < failureLimit)
            {
                var cell = new Tuple<int, int>(
                    random.Next(1, boardWidth - 1), random.Next(1, boardHeigth - 1));

                if (!cellList.Contains(cell) && cellList
                    .All(c => radialDistance(c,cell) > radius))
                {
                    cellList.Add(cell);
                    ++cellsCounter;
                }
                else
                    ++failureCounter;
            }

            var randomColor = new Random();
            foreach (var cell in cellList)
                Grid.board[cell.Item1, cell.Item2] = (byte)randomColor.Next(1, Queue.CurrentColorPool + 1);

            DrawBoard();
        }

        private double radialDistance(Tuple<int, int> a, Tuple<int, int> b)
        {
            return Math.Sqrt(Math.Pow(Math.Abs(a.Item1 - b.Item1), 2) + Math.Pow(Math.Abs(a.Item2 - b.Item2), 2));
        }

        private void checkBox1_CheckedChanged_1(object sender, EventArgs e)
        {
            if(checkBox1.Checked)
            {
                numericUpDown2.Enabled = true;
                ContinousGrainRandomization = true;
            }
            else
            {
                numericUpDown2.Enabled = false;
                ContinousGrainRandomization = false;
            }
        }

        private void addNewGrains(int n)
        {
            int cellsDemand = n, 
                cellsCounter = 0,
                failureCounter = 0,
                failureLimit = 200;

            int boardWidth = Grid.board.GetLength(0),
                boardHeigth = Grid.board.GetLength(1);
            Random random = new Random(),
                randomGrain = new Random();

            while (cellsCounter < cellsDemand && failureCounter < failureLimit)
            {
                var x = random.Next(1, boardWidth - 1);
                var y = random.Next(1, boardHeigth - 1);
                if (Grid.board[x, y] == 0)
                {
                    Grid.board[x, y] = (short)randomGrain.Next(1, Queue.CurrentColorPool + 1);
                    ++cellsCounter;
                }
                else
                    ++failureCounter;
            }

            DrawBoard();
        }

        private void recrystalizeButton_Click(object sender, EventArgs e)
        {
            currentIteration = 0;
            Evolver.CopyToTemporaryBoard();
            List<Tuple<Point, short>> randomPoints = null;
            if (constantNucleationRadio.Checked)
            {
                if (!int.TryParse(constanNucleationTextBox.Text, out int nucleonsCount))
                {
                    Debug.WriteLine("Failed to parse constant nucleation count");
                    return;
                }
                randomPoints = bordersOnlyRadio.Checked
                    ? GenerateRandomRecrystalizedNucleons(nucleonsCount, 500, RandomBorderLocationGenerator)
                    : GenerateRandomRecrystalizedNucleons(nucleonsCount, 200, RandomLocationGenerator);
            }
            Evolver.Grid.SetRecrystalizaitonFlags(false);

            Queue = new ColorQueue(Queue.CurrentColorPool, RedPaletteGenerator);
            colourBox_Click(colourBox, null);

            Evolver.Grid.SetGrains(0);

            if (randomPoints != null)
            {
                SetRecrystalizedCells(Evolver.Grid, randomPoints);
            }

            simulationBackground = new Bitmap(pictureBox.Image);
            pictureBox.Image = convertToGrayScale(simulationBackground);
            DrawBoard();

            recrystalizationTimer.Start();
        }

        private void recrystalizationTimer_Tick(object sender, EventArgs e)
        {
            if (incrementalNucleationRadio.Checked && currentIteration % 2 == 0)
            {
                var result = int.TryParse(incrementalNucleationTextBox.Text, out int nucleonDemand);
                if (result)
                {
                    var randomPoints = bordersOnlyRadio.Checked
                                ? GenerateRandomRecrystalizedNucleons(nucleonDemand, 500, RandomBorderLocationGenerator)
                                : GenerateRandomRecrystalizedNucleons(nucleonDemand, 200, RandomLocationGenerator);

                    SetRecrystalizedCells(Evolver.Grid, randomPoints);
                }
                else
                {
                    Debug.WriteLine("Failed to parse increasing nucleation count");
                }
            }

            if (Evolver.EvolveRecrystalization() == false)
            {
                recrystalizationTimer.Stop();
                recrystalizeButton.Enabled = false;
                recrystalizeButton.Enabled = true;
                var colorPool = Queue.CurrentColorPool;
                Queue = new ColorQueue(colorPool);
                colourBox_Click(colourBox, null);
                return;
            }
            
            currentIteration++;
            pictureBox.Image = simulationBackground;
            DrawBoard();
            DrawEnergy();
        }

        private void SetRecrystalizedCells(Grid grid, List<Tuple<Point, short>> cellList)
        {
            foreach (var pair in cellList)
            {
                var point = pair.Item1;
                var grainId = pair.Item2;
                grid.board[point.X, point.Y] = grainId;
                grid.energy[point.X, point.Y] = 0;
                grid.IsRecrystalized[point.X, point.Y] = true;
            }
        }

        private void recryStopButton_Click(object sender, EventArgs e)
        {
            recrystalizationTimer.Stop();
            recrystalizeButton.Enabled = true;
            recryStopButton.Enabled = false;
        }

        private void mcButton_Click(object sender, EventArgs e)
        {
            if (monteCarloTimer.Enabled)
            {
                monteCarloTimer.Stop();
                mcButton.Text = "Monte Carlo";
                DrawBoard();
                iterationLimit = 0;
                currentIteration = 0;
                iterationCounterLabel.Enabled = false;
            }
            else
            {
                var temp = grainRatioNumeric.Value;

                iterationLimit = (int)numericMonteCarlo.Value;
                iterationCounterLabel.Enabled = true;
                //grainRatioNumeric.Value = grainRatioNumeric.Minimum;
                //randomButton.PerformClick();
                //DrawBoard();
                //grainRatioNumeric.Value = temp;
                monteCarloTimer.Start();
                mcButton.Text = "Stop";
                mcButton.Enabled = true;

            }

            //mcButton.Enabled = false;
        }

        private void monteCarloTimer_Tick(object sender, EventArgs e)
        {
            if (currentIteration < iterationLimit)
            {
                Evolver.EvolveMonteCarlo(Queue.CurrentColorPool, ignoredGrainIds);
                ++currentIteration;
            }
            else
            {
                mcButton.Text = "Monte Carlo";
                currentIteration = 0;
                monteCarloTimer.Stop();
                mcButton.Enabled = true;
                iterationLimit = 0;
                iterationCounterLabel.Enabled = false;
            }
            iterationCounterLabel.Text = currentIteration.ToString();
            DrawBoard();
        }

        private void SaveAsBitmapMenuItem(object sender, EventArgs e)
        {
            var savefile = new SaveFileDialog();
            savefile.Filter = "Bitmaps (*.bmp)|*.bmp";
            savefile.ShowDialog();
            if(savefile.FileName != string.Empty)
                new GridFormatter().ToBitmap(Grid, Queue.GetColours())
                    .Save(savefile.FileName);
        }

        private void SaveAsTextMenuItem(object sender, EventArgs e)
        {
            var savefile = new SaveFileDialog();
            savefile.Filter = "Text (*.txt)|*.txt";
            savefile.ShowDialog();
            if (savefile.FileName != string.Empty)
            {
                var text = new GridFormatter().ToStringBuilder(Grid, Queue.GetColours()).ToString();
                File.WriteAllText(savefile.FileName, text);
            }
        }

        private void ImportFromTextMenuItem(object sender, EventArgs e)
        {
            var openfile = new OpenFileDialog();
            openfile.Filter = "Text (*.txt)|*.txt";
            openfile.ShowDialog();
            if(openfile.FileName != string.Empty)
            {
                var fileContents = File.ReadAllText(openfile.FileName);
                var import = new GridFormatter().FromText(fileContents);
                Grid = import.Item1;
                Queue = new ColorQueue(import.Item2);
                Evolver = new Evolver(Grid, Queue.CurrentColorPool);
                var neighbourhood = currentNeighbourhood();
                //neighbourhood.Idx = currentIndexTranslator();
                Evolver.Neighbourhood = neighbourhood;

                numericUpDown1.Value = Queue.CurrentColorPool;
                using (Graphics g = Graphics.FromImage(colourBox.Image))
                {
                    g.Clear(Queue.GetColorAndForward());
                }
                DrawBoard();
            }

        }

        private void ImportFromBitmapMenuItem(object sender, EventArgs e)
        {
            var openfile = new OpenFileDialog();
            openfile.Filter = "Bitmap (*.bmp)|*.bmp";
            openfile.ShowDialog();
            if (openfile.FileName != string.Empty)
            {
                var bitmap = new Bitmap(openfile.FileName);
                var import = new GridFormatter().FromImage(bitmap);

                Grid = import.Item1;
                Queue = new ColorQueue(import.Item2);
                Evolver = new Evolver(Grid, Queue.CurrentColorPool);
                var neighbourhood = currentNeighbourhood();
                //neighbourhood.Idx = currentIndexTranslator();
                Evolver.Neighbourhood = neighbourhood;

                numericUpDown1.Value = Queue.CurrentColorPool;
                using (Graphics g = Graphics.FromImage(colourBox.Image))
                {
                    g.Clear(Queue.GetColorAndForward());
                }
                DrawBoard();
            }
        }

        private void mooreNumeric_ValueChanged(object sender, EventArgs e)
        {
            stopButton.PerformClick();
            var indexTrans = Evolver.Neighbourhood.Idx;
            Evolver.Neighbourhood = new MooreNeighbourhood(indexTrans, (int)mooreNumeric.Value);
        }

        private void selectGrainButton_Click(object sender, EventArgs e)
        {
            ignoredGrainIds.Clear();
            tabControl1.SelectTab(0);
            var selected = grainsListBox.SelectedItems;
            var selectedGrainIds = selected.Cast<GrainItem>()
                .Select(item => item.GrainId)
                .ToList();
            

            for (var i = 1; i < Grid.board.GetLength(0) - 1; ++i)
                for (var j = 1; j < Grid.board.GetLength(1) - 1; ++j)
                {
                    var current = Grid.board[i, j];
                    if (!selectedGrainIds.Contains(current) && current != -1) Grid.board[i, j] = 0;
                }

            clearPictureBox();
            DrawBoard();
            if (dualPhaseCheckBox.Checked)
            {
                for (int i = 1; i < Grid.board.GetLength(0) - 1; ++i)
                    for (int j = 1; j < Grid.board.GetLength(1) - 1; ++j)
                        if (Grid.board[i, j] > 0)
                            Grid.board[i, j] = (short)(Queue.CurrentColor + 1);
                ignoredGrainIds.Add((short)(Queue.CurrentColor + 1));
            }
            else
                ignoredGrainIds = selectedGrainIds;

            clearPictureBox();
            DrawBoard();
            if (randomizeCheckBox.Checked)
            {
                RandomizeDomain(mcRandomizeCheckbox.Checked, true);
            }

            clearPictureBox();
            DrawBoard();
        }

        private void detectEdgesButton_Click(object sender, EventArgs e)
        {
            var temp = new short[Grid.board.GetLength(0), Grid.board.GetLength(1)];
            var nbhood = new MooreNeighbourhood(Evolver.Neighbourhood.Idx);
            for (int i = 1; i < Grid.board.GetLength(0) - 1; ++i)
                for (int j = 1; j < Grid.board.GetLength(1) - 1; ++j)
                {
                    if (nbhood.IsOneTheEdge(Grid.board, new Point(i, j)))
                        temp[i, j] = (short)-1;
                }

            Grid.board = temp;

            if (randomizeCheckBox.Checked)
            {
                var ratio = (double)grainRatioNumeric.Value;
                for (int i = 1; i < Grid.board.GetLength(0) - 1; ++i)
                    for (int j = 1; j < Grid.board.GetLength(1) - 1; ++j)
                    {
                        if (Grid.board[i, j] != -1 && random.NextDouble() >= ratio)
                            Grid.board[i, j] = (short)random.Next(1, Queue.CurrentColorPool + 1);
                    }
            }

            clearPictureBox();
            DrawBoard();
        }

        private void mcRandomizeCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            if (mcRandomizeCheckbox.Checked)
                grainRatioNumeric.Enabled = false;
            else
                grainRatioNumeric.Enabled = true;
        }

        private void homogenRadio_CheckedChanged(object sender, EventArgs e)
        {
            highEnergyNumericUpDown3.Enabled = false;
            highEnergyNumericUpDown3.Visible = false;

            label11.Enabled = false;
            label11.Visible = false;
        }

        private void heterogenRadio_CheckedChanged(object sender, EventArgs e)
        {
            highEnergyNumericUpDown3.Enabled = true;
            highEnergyNumericUpDown3.Visible = true;

            label11.Enabled = true;
            label11.Visible = true;
        }

        private void distributeEnergyButton_Click(object sender, EventArgs e)
        {
            DistributeEnergy(Grid, Evolver.Neighbourhood);
            DrawEnergy();
            recrystalizeButton.Enabled = true;
        }

        private Color RedPaletteGenerator()
        {
            var color = Color.FromArgb(random.Next(127, 256), random.Next(0, 256), 0);
            return color;
        }

        private void constantNucleationRadio_CheckedChanged(object sender, EventArgs e)
        {
            incrementalNucleationTextBox.Enabled = false;
            constanNucleationTextBox.Enabled = true;
        }

        private void incrementalNucleationRadio_CheckedChanged(object sender, EventArgs e)
        {
            incrementalNucleationTextBox.Enabled = true;
            constanNucleationTextBox.Enabled = false;
        }

        private List<Tuple<Point, short>> GenerateRandomRecrystalizedNucleons(int count, int attemptsLimit, Func<int, Tuple<bool, Point>> generator)
        {
            var grainPool = Queue.CurrentColorPool;
            int counter = 0;
            int succesful = currentIteration;
            var points = new List<Tuple<Point, short>>(count);
            while(counter < count)
            {
                var result = generator(attemptsLimit);
                if (result.Item1 == false)
                    break;
                var point = result.Item2;
                var grainId = (short)((succesful++ % grainPool) + 1);
                points.Add(new Tuple<Point, short>(point, grainId));
                ++counter;
                Debug.WriteLine($"Generated grain: {grainId} in point [{point.X}, {point.Y}]");
            }
            return points;
        }

        private Tuple<bool, Point> RandomLocationGenerator(int attemptsLimit)
        {
            var isRecrystalized = Evolver.Grid.IsRecrystalized;
            for(var i = 0; i < attemptsLimit; ++i)
            {
                var p = new Point(
                    random.Next(1, 201),
                    random.Next(1, 101)
                    );
                if(!isRecrystalized[p.X, p.Y])
                {
                    return new Tuple<bool, Point>(true, p);
                }
            }
            Debug.WriteLine("Random location generator failed");
            return new Tuple<bool, Point>(false, Point.Empty);
        }

        private Tuple<bool, Point> RandomBorderLocationGenerator(int attemptsLimit)
        {
            var isRecrystalized = Evolver.Grid.IsRecrystalized;
            var board = Evolver.Grid.board;
            var moore = new MooreNeighbourhood(Evolver.Neighbourhood.Idx);

            for(var i = 0; i < attemptsLimit; ++i)
            {
                var p = new Point(
                    random.Next(1, 201),
                    random.Next(1, 101)
                    );
                if(!isRecrystalized[p.X, p.Y])
                {
                    if (moore.IsOneTheEdge(board, p) && moore.IsOneTheEdge(Evolver.temporaryBoard, p))
                        return new Tuple<bool, Point>(true, p);
                }
            }
            Debug.WriteLine("Random border generator failed");
            return new Tuple<bool, Point>(false, Point.Empty);
        }

        private Bitmap convertToGrayScale(Bitmap bmp)
        {
            for (int i = 0; i < bmp.Width; i++)
                for (int j = 0; j < bmp.Height; j++)
                {
                    Color c = bmp.GetPixel(i, j);
                    byte gray = (byte)(.21 * c.R + .71 * c.G + .071 * c.B);
                    bmp.SetPixel(i, j, Color.FromArgb(gray, gray, gray));
                }

            return bmp;
        }
    }
}
