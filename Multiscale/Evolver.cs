﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;

namespace Multiscale
{
    class Evolver
    {
        public Neighbourhood Neighbourhood { get; set; }
        public Grid Grid { get; set; }
        public short[,] temporaryBoard;
        private Random random = new Random();
        private List<Point> subsequentPoints;
        private List<Point> unrecrystalizedPoints;

        public Evolver(Grid grid, int grainTypes)
        {
            Grid = grid;
            Neighbourhood = new MooreNeighbourhood(new NonperiodicIndexTranslator());
            temporaryBoard = new short[grid.board.GetLength(0), grid.board.GetLength(1)];
            subsequentPoints = new List<Point>((grid.board.GetLength(0) - 2)
                                                * (grid.board.GetLength(1) - 2));
            unrecrystalizedPoints = new List<Point>();
            var boardWidth = Grid.board.GetLength(0);
            var boardHeigth = Grid.board.GetLength(1);
            for (int x = 1; x < boardWidth - 1; ++x)
                for (int y = 1; y < boardHeigth - 1; ++y)
                    subsequentPoints.Add(new Point(x, y));
        }

        public Evolver(Grid grid, Neighbourhood neighbourhood)
        {
            Grid = grid;
            Neighbourhood = neighbourhood;
            temporaryBoard = new short[grid.board.GetLength(0), grid.board.GetLength(1)];
        }

        public void CopyToTemporaryBoard()
        {
            var boardWidth = Grid.board.GetLength(0);
            var boardHeigth = Grid.board.GetLength(1);
            for (int x = 1; x < boardWidth - 1; ++x)
                for (int y = 1; y < boardHeigth - 1; ++y)
                    temporaryBoard[x, y] = Grid.board[x, y];
        }

        public bool Evolve(List<short> ignoredGrainIds)
        {
            var boardWidth = Grid.board.GetLength(0);
            var boardHeigth = Grid.board.GetLength(1);
            
            for (int x = 1; x < boardWidth - 1; ++x)
                for (int y = 1; y < boardHeigth - 1; ++y)
                    temporaryBoard[x, y] = Grid.board[x, y];

            var isStateChanged = false;

            for (int x = 1; x < boardWidth - 1; ++x)
                for (int y = 1; y < boardHeigth - 1; ++y)
                    if (Grid.board[x, y] == 0)
                    {
                        temporaryBoard[x, y] = (short)Neighbourhood.GetCrystalizationResult(Grid.board, new Point(x, y), ignoredGrainIds);
                        isStateChanged = true;
                    }
            
            for (int x = 1; x < boardWidth - 1; ++x)
                for (int y = 1; y < boardHeigth - 1; ++y)
                    Grid.board[x, y] = temporaryBoard[x, y];
            return isStateChanged;
        }

        public void EvolveMonteCarlo(int grainsPoolCount, List<short> ignoredGrainIds)
        {
            var boardWidth = Grid.board.GetLength(0);
            var boardHeigth = Grid.board.GetLength(1);

            for (int x = 1; x < boardWidth - 1; ++x)
                for (int y = 1; y < boardHeigth - 1; ++y)
                    temporaryBoard[x, y] = Grid.board[x, y];

            subsequentPoints = subsequentPoints.Shuffle(random).ToList();
            foreach (var cell in subsequentPoints)
            {
                var grainId = Grid.board[cell.X, cell.Y];
                if (grainId <= 0 || ignoredGrainIds.Contains(grainId))
                    continue;
                var energy = Neighbourhood.CountDifferentGrains(Grid.board, grainId, cell);

                short randomNeighbour = Neighbourhood.RandomNeighbour(Grid.board, cell, random);
                if (randomNeighbour <= 0)
                    continue;
                if (ignoredGrainIds.Contains(randomNeighbour))
                    continue;
                var newEnergy = Neighbourhood.CountDifferentGrains(Grid.board, randomNeighbour, cell);
                if(newEnergy <= energy)
                    temporaryBoard[cell.X, cell.Y] = randomNeighbour;
            }

            for (int x = 1; x < boardWidth - 1; ++x)
                for (int y = 1; y < boardHeigth - 1; ++y)
                    Grid.board[x, y] = temporaryBoard[x, y];
        }

        public bool EvolveRecrystalization()
        {
            var boardWidth = Grid.board.GetLength(0);
            var boardHeigth = Grid.board.GetLength(1);

            unrecrystalizedPoints.Clear();
            for (int x = 1; x < boardWidth - 1; ++x)
                for (int y = 1; y < boardHeigth - 1; ++y)
                    if (Grid.IsRecrystalized[x, y] == false)
                        unrecrystalizedPoints.Add(new Point(x, y));

            if (unrecrystalizedPoints.Count == 0)
                return false;

            unrecrystalizedPoints = unrecrystalizedPoints.Shuffle(random).ToList();
            foreach (var point in unrecrystalizedPoints)
            {
                var moore = new MooreNeighbourhood(Neighbourhood.Idx);

                int currentEnergy = Grid.energy[point.X, point.Y];
                var currentGrain = Grid.board[point.X, point.Y];
                var kroneckerDelta = moore.CountDifferentGrains(Grid.board, currentGrain, point);
                currentEnergy += kroneckerDelta;
                if(currentEnergy > 0 /*&& kroneckerDelta > 0*/)
                {
                    var foundGrain = moore.TryGetRandomRecrystalizedNeighbour(
                        Grid.board, Grid.IsRecrystalized, 
                        point, random, out short recrystalizedGrain
                        );
                    if (!foundGrain) continue;
                    int newEnergy = moore.CountDifferentGrains(Grid.board, recrystalizedGrain, point);
                    if(currentEnergy >= newEnergy)
                    {
                        Grid.board[point.X, point.Y] = recrystalizedGrain;
                        Grid.energy[point.X, point.Y] = 0;
                        Grid.IsRecrystalized[point.X, point.Y] = true;
                    }    
                }
            }

            return true;
        }
    }
}
