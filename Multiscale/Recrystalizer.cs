﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Diagnostics;

namespace Multiscale
{
    class Recrystalizer
    {
        public bool[,] Crystalized { get; set; }
        public float[,] Dislocation { get; set; }
        private Evolver evolver;
        private ColorQueue queue;
        private readonly Equation eq = new Equation(86710969050178.5f, 9.41268203527779f);
        private readonly HashSet<Point> edgeCells;
        private Random rand = new Random();

        private int crystalizedCount = 0;
        private readonly int kFactor;
        private float prevRho = 0.0f;
        private float currentTime = 0.0f;
        private readonly float dTime = 0.01f;
        private readonly float criticalRho;
        
        public Recrystalizer(Evolver evolver, ColorQueue colorQueue, int k)
        {
            kFactor = k;
            queue = colorQueue;
            this.evolver = evolver;
            var dim = new Tuple<int, int>(evolver.Grid.board.GetLength(0), evolver.Grid.board.GetLength(1));
            Crystalized = new bool[dim.Item1, dim.Item2];
            Dislocation = new float[dim.Item1, dim.Item2];
            criticalRho = eq.Value(0.065f) / (dim.Item1 * dim.Item2);
            Console.WriteLine(criticalRho);

            edgeCells = new HashSet<Point>();
            var board = evolver.Grid.board;
            for (int x = 1; x < dim.Item1 - 1; ++x)
                for (int y = 1; y < dim.Item2 - 1; ++y)
                {
                    var color = board[x, y];
                    if (board[x - 1, y] != 0 && board[x - 1, y] != color) { edgeCells.Add(new Point(x, y)); continue; }
                    if (board[x + 1, y] != 0 && board[x + 1, y] != color) { edgeCells.Add(new Point(x, y)); continue; }
                    for (var i = -1; i <= 1; ++i)
                        if (board[x + i, y - 1] != 0 && board[x + i, y - 1] != color) { edgeCells.Add(new Point(x, y)); continue; }
                    for (var i = -1; i <= 1; ++i)
                        if (board[x + i, y + 1] != 0 && board[x + i, y + 1] != color) { edgeCells.Add(new Point(x, y)); continue; }

                }

            evolver.Grid.board = new short[dim.Item1, dim.Item2];
        }

        public void Recrystalize()
        {
            var Rho = eq.Value(currentTime);
            var deltaRho = Rho - prevRho;
            var remainingRho = DistributeDislocations(deltaRho);
            DirstibuteRemainingDislocations(remainingRho);
            RecrystalizeCriticalCells();
            evolver.Evolve(new List<short>() { });

            prevRho = Rho;
            currentTime += dTime;
        }

        private float DistributeDislocations(float deltaRho)
        {
            var rhoPerCell = deltaRho / (evolver.Grid.board.GetLength(0) * evolver.Grid.board.GetLength(1) - crystalizedCount);
            var distributedRho = 0.0f;
            
            for (int x = 1; x < evolver.Grid.board.GetLength(0) - 1; ++x)
                for (int y = 1; y < evolver.Grid.board.GetLength(1) - 1; ++y)
                {
                    var packet = edgeCells.Contains(new Point(x, y))
                            ? 0.8f * rhoPerCell
                            : 0.2f * rhoPerCell;

                    Dislocation[x, y] += packet;
                    distributedRho += packet;
                }

            return deltaRho - distributedRho;
        }

        private void DirstibuteRemainingDislocations(float remainingDislocations)
        {
            var packet = remainingDislocations / kFactor / edgeCells.Count;
            foreach (var point in edgeCells)
                Dislocation[point.X, point.Y] += packet;
        }

        private void RecrystalizeCriticalCells()
        {
            for (int x = 1; x < evolver.Grid.board.GetLength(0) - 1; ++x)
                for (int y = 1; y < evolver.Grid.board.GetLength(1) - 1; ++y)
                {
                    if(Dislocation[x,y] > criticalRho && !Crystalized[x, y])
                    {
                        Dislocation[x, y] = 0;
                        Crystalized[x, y] = true;
                        queue.AddNewColor();
                        var color = (short)queue.GetLatestCrystalAsNumber();
                        evolver.Grid.board[x, y] = color;
                    }
                }
        }

        private void ApplyRecrystalizationToNeighbourhood(short color, Point point)
        {
            var Idx = evolver.Neighbourhood.Idx;
            var x = point.X; var y = point.Y;
            var board = evolver.Grid.board;

            if(board[Idx.X(x - 1), Idx.Y(y)] == 0)
            {
                board[Idx.X(x - 1), Idx.Y(y)] = color;
                Crystalized[Idx.X(x - 1), Idx.Y(y)] = true;
                Dislocation[Idx.X(x - 1), Idx.Y(y)] = 0;
            }
            if (board[Idx.X(x + 1), Idx.Y(y)] == 0)
            {
                board[Idx.X(x + 1), Idx.Y(y)] = color;
                Crystalized[Idx.X(x + 1), Idx.Y(y)] = true;
                Dislocation[Idx.X(x + 1), Idx.Y(y)] = 0;
            }
            for (var i = -1; i <= 1; ++i)
                if (board[Idx.X(x + i), Idx.Y(y - 1)] == 0)
                {
                    board[Idx.X(x + i), Idx.Y(y - 1)] = color;
                    Crystalized[Idx.X(x + i), Idx.Y(y - 1)] = true;
                    Dislocation[Idx.X(x + i), Idx.Y(y - 1)] = 0;
                }
            for (var i = -1; i <= 1; ++i)
                if(board[Idx.X(x + i), Idx.Y(y + 1)] == 0)
                {
                    board[Idx.X(x + i), Idx.Y(y + 1)] = color;
                    Crystalized[Idx.X(x + i), Idx.Y(y + 1)] = true;
                    Dislocation[Idx.X(x + i), Idx.Y(y + 1)] = 0;
                }
                
        }

        private class Equation
        {
            private readonly float A;
            private readonly float B;

            public Equation(float A, float B)
            {
                this.A = A;
                this.B = B;
            }

            public float Value(float t)
            {
                return A / B + (1.0f - A / B) * (float)Math.Pow(Math.E, -B * t);
            }
        }
    }
}
