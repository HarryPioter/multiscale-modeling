﻿using System;
using System.Drawing;

namespace Multiscale
{
    class ClickGrid
    {
        private readonly Size pictureBoxSize;
        private readonly Size boardSize;
        private readonly float pixelDx;
        private readonly float pixelDy;

        public ClickGrid(Size pictureBoxSize, Size boardSize)
        {
            this.pictureBoxSize = pictureBoxSize;
            this.boardSize = boardSize;
            pixelDx = pictureBoxSize.Width / (float)boardSize.Width;
            pixelDy = pictureBoxSize.Height / (float)boardSize.Height;
        }

        public Tuple<int, int> getCellIndices(int x, int y)
        {
            var xIndex = (int)(x / pixelDx);
            var yIndex = (int)(y / pixelDy);

            return new Tuple<int, int>(xIndex, yIndex);
        }
    }
}
