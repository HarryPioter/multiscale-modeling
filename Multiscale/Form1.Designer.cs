﻿namespace Multiscale
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.colourBox = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.mcRandomizeCheckbox = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.radiusNumeric = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.radiusButton = new System.Windows.Forms.Button();
            this.evenNumeric = new System.Windows.Forms.NumericUpDown();
            this.button2 = new System.Windows.Forms.Button();
            this.grainRatioNumeric = new System.Windows.Forms.NumericUpDown();
            this.randomButton = new System.Windows.Forms.Button();
            this.recrystalizationTimer = new System.Windows.Forms.Timer(this.components);
            this.recrystalizeButton = new System.Windows.Forms.Button();
            this.kNumeric = new System.Windows.Forms.NumericUpDown();
            this.recryStopButton = new System.Windows.Forms.Button();
            this.numericMonteCarlo = new System.Windows.Forms.NumericUpDown();
            this.mcButton = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.monteCarloTimer = new System.Windows.Forms.Timer(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.iterationCounterLabel = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.imageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bitmapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.textToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importFromToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imageToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.textToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.inclusionsNumeric = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.radialInclusionRadioButton = new System.Windows.Forms.RadioButton();
            this.squareInclusionRadioButton = new System.Windows.Forms.RadioButton();
            this.thridTabPage = new System.Windows.Forms.TabPage();
            this.selectGroupBox = new System.Windows.Forms.GroupBox();
            this.detectEdgesButton = new System.Windows.Forms.Button();
            this.randomizeCheckBox = new System.Windows.Forms.CheckBox();
            this.dualPhaseCheckBox = new System.Windows.Forms.CheckBox();
            this.selectGrainButton = new System.Windows.Forms.Button();
            this.grainsListBox = new System.Windows.Forms.ListBox();
            this.firstTabPage = new System.Windows.Forms.TabPage();
            this.neighbourhoodGroupBox = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.mooreNumeric = new System.Windows.Forms.NumericUpDown();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.radioButton8 = new System.Windows.Forms.RadioButton();
            this.radioButton7 = new System.Windows.Forms.RadioButton();
            this.startButton = new System.Windows.Forms.Button();
            this.stopButton = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.clearButton = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.energyBox = new System.Windows.Forms.PictureBox();
            this.energyGroupBox = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.highEnergyNumericUpDown3 = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.lowEnergyNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.heterogenRadio = new System.Windows.Forms.RadioButton();
            this.homogenRadio = new System.Windows.Forms.RadioButton();
            this.distributeEnergyButton = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.constantNucleationRadio = new System.Windows.Forms.RadioButton();
            this.incrementalNucleationRadio = new System.Windows.Forms.RadioButton();
            this.constanNucleationTextBox = new System.Windows.Forms.TextBox();
            this.incrementalNucleationTextBox = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.bordersOnlyRadio = new System.Windows.Forms.RadioButton();
            this.unconstrainedRadio = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.colourBox)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radiusNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.evenNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grainRatioNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMonteCarlo)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.inclusionsNumeric)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.thridTabPage.SuspendLayout();
            this.selectGroupBox.SuspendLayout();
            this.firstTabPage.SuspendLayout();
            this.neighbourhoodGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mooreNumeric)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.energyBox)).BeginInit();
            this.energyGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.highEnergyNumericUpDown3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lowEnergyNumericUpDown)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // timer
            // 
            this.timer.Interval = 50;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // pictureBox
            // 
            this.pictureBox.Location = new System.Drawing.Point(12, 33);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(600, 300);
            this.pictureBox.TabIndex = 0;
            this.pictureBox.TabStop = false;
            this.pictureBox.Click += new System.EventHandler(this.pictureBox_Click);
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(6, 19);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(62, 20);
            this.numericUpDown1.TabIndex = 1;
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.colourBox);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.numericUpDown1);
            this.groupBox1.Location = new System.Drawing.Point(622, 321);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(158, 80);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Grains";
            // 
            // colourBox
            // 
            this.colourBox.Location = new System.Drawing.Point(90, 19);
            this.colourBox.Name = "colourBox";
            this.colourBox.Size = new System.Drawing.Size(50, 50);
            this.colourBox.TabIndex = 3;
            this.colourBox.TabStop = false;
            this.colourBox.Click += new System.EventHandler(this.colourBox_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Different grains";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.mcRandomizeCheckbox);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.numericUpDown2);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.checkBox1);
            this.groupBox3.Controls.Add(this.radiusNumeric);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.radiusButton);
            this.groupBox3.Controls.Add(this.evenNumeric);
            this.groupBox3.Controls.Add(this.button2);
            this.groupBox3.Controls.Add(this.grainRatioNumeric);
            this.groupBox3.Controls.Add(this.randomButton);
            this.groupBox3.Location = new System.Drawing.Point(12, 343);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(287, 69);
            this.groupBox3.TabIndex = 9;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Randomize";
            // 
            // mcRandomizeCheckbox
            // 
            this.mcRandomizeCheckbox.AutoSize = true;
            this.mcRandomizeCheckbox.Location = new System.Drawing.Point(7, 49);
            this.mcRandomizeCheckbox.Name = "mcRandomizeCheckbox";
            this.mcRandomizeCheckbox.Size = new System.Drawing.Size(72, 17);
            this.mcRandomizeCheckbox.TabIndex = 14;
            this.mcRandomizeCheckbox.Text = "MC Mode";
            this.mcRandomizeCheckbox.UseVisualStyleBackColor = true;
            this.mcRandomizeCheckbox.CheckedChanged += new System.EventHandler(this.mcRandomizeCheckbox_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(171, 141);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Grains per tick";
            this.label5.Visible = false;
            // 
            // numericUpDown2
            // 
            this.numericUpDown2.Enabled = false;
            this.numericUpDown2.Location = new System.Drawing.Point(119, 140);
            this.numericUpDown2.Name = "numericUpDown2";
            this.numericUpDown2.Size = new System.Drawing.Size(44, 20);
            this.numericUpDown2.TabIndex = 12;
            this.numericUpDown2.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown2.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(150, 115);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Radius";
            this.label4.Visible = false;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(6, 140);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(106, 17);
            this.checkBox1.TabIndex = 10;
            this.checkBox1.Text = "Continous Grains";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.Visible = false;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged_1);
            // 
            // radiusNumeric
            // 
            this.radiusNumeric.Location = new System.Drawing.Point(88, 113);
            this.radiusNumeric.Name = "radiusNumeric";
            this.radiusNumeric.Size = new System.Drawing.Size(56, 20);
            this.radiusNumeric.TabIndex = 7;
            this.radiusNumeric.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.radiusNumeric.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(169, 86);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Grains";
            this.label3.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(150, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Grains/Empty ratio";
            // 
            // radiusButton
            // 
            this.radiusButton.Location = new System.Drawing.Point(7, 111);
            this.radiusButton.Name = "radiusButton";
            this.radiusButton.Size = new System.Drawing.Size(75, 23);
            this.radiusButton.TabIndex = 4;
            this.radiusButton.Text = "Radial";
            this.radiusButton.UseVisualStyleBackColor = true;
            this.radiusButton.Visible = false;
            this.radiusButton.Click += new System.EventHandler(this.radiusButton_Click);
            // 
            // evenNumeric
            // 
            this.evenNumeric.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.evenNumeric.Location = new System.Drawing.Point(88, 83);
            this.evenNumeric.Name = "evenNumeric";
            this.evenNumeric.Size = new System.Drawing.Size(75, 20);
            this.evenNumeric.TabIndex = 3;
            this.evenNumeric.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.evenNumeric.Visible = false;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(7, 81);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "Even";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // grainRatioNumeric
            // 
            this.grainRatioNumeric.DecimalPlaces = 3;
            this.grainRatioNumeric.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.grainRatioNumeric.Location = new System.Drawing.Point(88, 23);
            this.grainRatioNumeric.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            196608});
            this.grainRatioNumeric.Name = "grainRatioNumeric";
            this.grainRatioNumeric.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.grainRatioNumeric.Size = new System.Drawing.Size(56, 20);
            this.grainRatioNumeric.TabIndex = 1;
            this.grainRatioNumeric.Value = new decimal(new int[] {
            999,
            0,
            0,
            196608});
            // 
            // randomButton
            // 
            this.randomButton.Location = new System.Drawing.Point(7, 20);
            this.randomButton.Name = "randomButton";
            this.randomButton.Size = new System.Drawing.Size(75, 23);
            this.randomButton.TabIndex = 0;
            this.randomButton.Text = "Random";
            this.randomButton.UseVisualStyleBackColor = true;
            this.randomButton.Click += new System.EventHandler(this.randomButton_Click);
            // 
            // recrystalizationTimer
            // 
            this.recrystalizationTimer.Interval = 300;
            this.recrystalizationTimer.Tick += new System.EventHandler(this.recrystalizationTimer_Tick);
            // 
            // recrystalizeButton
            // 
            this.recrystalizeButton.Enabled = false;
            this.recrystalizeButton.Location = new System.Drawing.Point(350, 452);
            this.recrystalizeButton.Name = "recrystalizeButton";
            this.recrystalizeButton.Size = new System.Drawing.Size(75, 23);
            this.recrystalizeButton.TabIndex = 10;
            this.recrystalizeButton.Text = "Recrystalize";
            this.recrystalizeButton.UseVisualStyleBackColor = true;
            this.recrystalizeButton.UseWaitCursor = true;
            this.recrystalizeButton.Click += new System.EventHandler(this.recrystalizeButton_Click);
            // 
            // kNumeric
            // 
            this.kNumeric.Location = new System.Drawing.Point(728, 452);
            this.kNumeric.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.kNumeric.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.kNumeric.Name = "kNumeric";
            this.kNumeric.Size = new System.Drawing.Size(74, 20);
            this.kNumeric.TabIndex = 11;
            this.kNumeric.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.kNumeric.Visible = false;
            // 
            // recryStopButton
            // 
            this.recryStopButton.Enabled = false;
            this.recryStopButton.Location = new System.Drawing.Point(727, 478);
            this.recryStopButton.Name = "recryStopButton";
            this.recryStopButton.Size = new System.Drawing.Size(75, 23);
            this.recryStopButton.TabIndex = 12;
            this.recryStopButton.Text = "Stop";
            this.recryStopButton.UseVisualStyleBackColor = true;
            this.recryStopButton.Visible = false;
            this.recryStopButton.Click += new System.EventHandler(this.recryStopButton_Click);
            // 
            // numericMonteCarlo
            // 
            this.numericMonteCarlo.Increment = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.numericMonteCarlo.Location = new System.Drawing.Point(468, 372);
            this.numericMonteCarlo.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericMonteCarlo.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericMonteCarlo.Name = "numericMonteCarlo";
            this.numericMonteCarlo.Size = new System.Drawing.Size(74, 20);
            this.numericMonteCarlo.TabIndex = 14;
            this.numericMonteCarlo.Value = new decimal(new int[] {
            300,
            0,
            0,
            0});
            // 
            // mcButton
            // 
            this.mcButton.Location = new System.Drawing.Point(467, 343);
            this.mcButton.Name = "mcButton";
            this.mcButton.Size = new System.Drawing.Size(75, 23);
            this.mcButton.TabIndex = 13;
            this.mcButton.Text = "Monte Carlo";
            this.mcButton.UseVisualStyleBackColor = true;
            this.mcButton.Click += new System.EventHandler(this.mcButton_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(548, 374);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Iterations";
            // 
            // monteCarloTimer
            // 
            this.monteCarloTimer.Interval = 25;
            this.monteCarloTimer.Tick += new System.EventHandler(this.monteCarloTimer_Tick);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(468, 399);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "Iteration: ";
            // 
            // iterationCounterLabel
            // 
            this.iterationCounterLabel.AutoSize = true;
            this.iterationCounterLabel.Location = new System.Drawing.Point(525, 399);
            this.iterationCounterLabel.Name = "iterationCounterLabel";
            this.iterationCounterLabel.Size = new System.Drawing.Size(0, 13);
            this.iterationCounterLabel.TabIndex = 17;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.imageToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(837, 24);
            this.menuStrip1.TabIndex = 18;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // imageToolStripMenuItem
            // 
            this.imageToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveAsToolStripMenuItem,
            this.importFromToolStripMenuItem});
            this.imageToolStripMenuItem.Name = "imageToolStripMenuItem";
            this.imageToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.imageToolStripMenuItem.Text = "Image";
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bitmapToolStripMenuItem,
            this.textToolStripMenuItem});
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.saveAsToolStripMenuItem.Text = "Save as...";
            // 
            // bitmapToolStripMenuItem
            // 
            this.bitmapToolStripMenuItem.Name = "bitmapToolStripMenuItem";
            this.bitmapToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.bitmapToolStripMenuItem.Text = "Bitmap";
            this.bitmapToolStripMenuItem.Click += new System.EventHandler(this.SaveAsBitmapMenuItem);
            // 
            // textToolStripMenuItem
            // 
            this.textToolStripMenuItem.Name = "textToolStripMenuItem";
            this.textToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.textToolStripMenuItem.Text = "Text";
            this.textToolStripMenuItem.Click += new System.EventHandler(this.SaveAsTextMenuItem);
            // 
            // importFromToolStripMenuItem
            // 
            this.importFromToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.imageToolStripMenuItem1,
            this.textToolStripMenuItem1});
            this.importFromToolStripMenuItem.Name = "importFromToolStripMenuItem";
            this.importFromToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.importFromToolStripMenuItem.Text = "Import from...";
            // 
            // imageToolStripMenuItem1
            // 
            this.imageToolStripMenuItem1.Name = "imageToolStripMenuItem1";
            this.imageToolStripMenuItem1.Size = new System.Drawing.Size(112, 22);
            this.imageToolStripMenuItem1.Text = "Bitmap";
            this.imageToolStripMenuItem1.Click += new System.EventHandler(this.ImportFromBitmapMenuItem);
            // 
            // textToolStripMenuItem1
            // 
            this.textToolStripMenuItem1.Name = "textToolStripMenuItem1";
            this.textToolStripMenuItem1.Size = new System.Drawing.Size(112, 22);
            this.textToolStripMenuItem1.Text = "Text";
            this.textToolStripMenuItem1.Click += new System.EventHandler(this.ImportFromTextMenuItem);
            // 
            // inclusionsNumeric
            // 
            this.inclusionsNumeric.Location = new System.Drawing.Point(71, 19);
            this.inclusionsNumeric.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.inclusionsNumeric.Name = "inclusionsNumeric";
            this.inclusionsNumeric.Size = new System.Drawing.Size(62, 20);
            this.inclusionsNumeric.TabIndex = 19;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(70, 43);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 13);
            this.label8.TabIndex = 20;
            this.label8.Text = "Inclusion size";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.radialInclusionRadioButton);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.squareInclusionRadioButton);
            this.groupBox4.Controls.Add(this.inclusionsNumeric);
            this.groupBox4.Location = new System.Drawing.Point(305, 339);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(156, 62);
            this.groupBox4.TabIndex = 21;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Inclusion";
            // 
            // radialInclusionRadioButton
            // 
            this.radialInclusionRadioButton.AutoSize = true;
            this.radialInclusionRadioButton.Location = new System.Drawing.Point(6, 39);
            this.radialInclusionRadioButton.Name = "radialInclusionRadioButton";
            this.radialInclusionRadioButton.Size = new System.Drawing.Size(55, 17);
            this.radialInclusionRadioButton.TabIndex = 23;
            this.radialInclusionRadioButton.TabStop = true;
            this.radialInclusionRadioButton.Text = "Radial";
            this.radialInclusionRadioButton.UseVisualStyleBackColor = true;
            // 
            // squareInclusionRadioButton
            // 
            this.squareInclusionRadioButton.AutoSize = true;
            this.squareInclusionRadioButton.Checked = true;
            this.squareInclusionRadioButton.Location = new System.Drawing.Point(6, 19);
            this.squareInclusionRadioButton.Name = "squareInclusionRadioButton";
            this.squareInclusionRadioButton.Size = new System.Drawing.Size(59, 17);
            this.squareInclusionRadioButton.TabIndex = 22;
            this.squareInclusionRadioButton.TabStop = true;
            this.squareInclusionRadioButton.Text = "Square";
            this.squareInclusionRadioButton.UseVisualStyleBackColor = true;
            // 
            // thridTabPage
            // 
            this.thridTabPage.AccessibleDescription = "";
            this.thridTabPage.Controls.Add(this.selectGroupBox);
            this.thridTabPage.Controls.Add(this.grainsListBox);
            this.thridTabPage.Location = new System.Drawing.Point(4, 22);
            this.thridTabPage.Name = "thridTabPage";
            this.thridTabPage.Size = new System.Drawing.Size(192, 256);
            this.thridTabPage.TabIndex = 2;
            this.thridTabPage.Text = "Grain select";
            this.thridTabPage.UseVisualStyleBackColor = true;
            // 
            // selectGroupBox
            // 
            this.selectGroupBox.Controls.Add(this.detectEdgesButton);
            this.selectGroupBox.Controls.Add(this.randomizeCheckBox);
            this.selectGroupBox.Controls.Add(this.dualPhaseCheckBox);
            this.selectGroupBox.Controls.Add(this.selectGrainButton);
            this.selectGroupBox.Location = new System.Drawing.Point(3, 169);
            this.selectGroupBox.Name = "selectGroupBox";
            this.selectGroupBox.Size = new System.Drawing.Size(186, 84);
            this.selectGroupBox.TabIndex = 23;
            this.selectGroupBox.TabStop = false;
            this.selectGroupBox.Text = "Selection";
            // 
            // detectEdgesButton
            // 
            this.detectEdgesButton.Location = new System.Drawing.Point(6, 48);
            this.detectEdgesButton.Name = "detectEdgesButton";
            this.detectEdgesButton.Size = new System.Drawing.Size(75, 23);
            this.detectEdgesButton.TabIndex = 4;
            this.detectEdgesButton.Text = "Edges";
            this.detectEdgesButton.UseVisualStyleBackColor = true;
            this.detectEdgesButton.Click += new System.EventHandler(this.detectEdgesButton_Click);
            // 
            // randomizeCheckBox
            // 
            this.randomizeCheckBox.AutoSize = true;
            this.randomizeCheckBox.Location = new System.Drawing.Point(87, 42);
            this.randomizeCheckBox.Name = "randomizeCheckBox";
            this.randomizeCheckBox.Size = new System.Drawing.Size(91, 17);
            this.randomizeCheckBox.TabIndex = 3;
            this.randomizeCheckBox.Text = "Re-randomize";
            this.randomizeCheckBox.UseVisualStyleBackColor = true;
            // 
            // dualPhaseCheckBox
            // 
            this.dualPhaseCheckBox.AutoSize = true;
            this.dualPhaseCheckBox.Location = new System.Drawing.Point(87, 19);
            this.dualPhaseCheckBox.Name = "dualPhaseCheckBox";
            this.dualPhaseCheckBox.Size = new System.Drawing.Size(81, 17);
            this.dualPhaseCheckBox.TabIndex = 2;
            this.dualPhaseCheckBox.Text = "Dual Phase";
            this.dualPhaseCheckBox.UseVisualStyleBackColor = true;
            // 
            // selectGrainButton
            // 
            this.selectGrainButton.Location = new System.Drawing.Point(6, 19);
            this.selectGrainButton.Name = "selectGrainButton";
            this.selectGrainButton.Size = new System.Drawing.Size(75, 23);
            this.selectGrainButton.TabIndex = 1;
            this.selectGrainButton.Text = "Select";
            this.selectGrainButton.UseVisualStyleBackColor = true;
            this.selectGrainButton.Click += new System.EventHandler(this.selectGrainButton_Click);
            // 
            // grainsListBox
            // 
            this.grainsListBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.grainsListBox.FormattingEnabled = true;
            this.grainsListBox.Location = new System.Drawing.Point(32, 3);
            this.grainsListBox.Name = "grainsListBox";
            this.grainsListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.grainsListBox.Size = new System.Drawing.Size(126, 160);
            this.grainsListBox.TabIndex = 0;
            // 
            // firstTabPage
            // 
            this.firstTabPage.Controls.Add(this.neighbourhoodGroupBox);
            this.firstTabPage.Controls.Add(this.groupBox2);
            this.firstTabPage.Controls.Add(this.startButton);
            this.firstTabPage.Controls.Add(this.stopButton);
            this.firstTabPage.Controls.Add(this.button1);
            this.firstTabPage.Controls.Add(this.clearButton);
            this.firstTabPage.Location = new System.Drawing.Point(4, 22);
            this.firstTabPage.Name = "firstTabPage";
            this.firstTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.firstTabPage.Size = new System.Drawing.Size(192, 256);
            this.firstTabPage.TabIndex = 0;
            this.firstTabPage.Text = "Control";
            this.firstTabPage.UseVisualStyleBackColor = true;
            // 
            // neighbourhoodGroupBox
            // 
            this.neighbourhoodGroupBox.Controls.Add(this.label9);
            this.neighbourhoodGroupBox.Controls.Add(this.mooreNumeric);
            this.neighbourhoodGroupBox.Controls.Add(this.radioButton2);
            this.neighbourhoodGroupBox.Controls.Add(this.radioButton1);
            this.neighbourhoodGroupBox.Location = new System.Drawing.Point(9, 65);
            this.neighbourhoodGroupBox.Name = "neighbourhoodGroupBox";
            this.neighbourhoodGroupBox.Size = new System.Drawing.Size(155, 94);
            this.neighbourhoodGroupBox.TabIndex = 6;
            this.neighbourhoodGroupBox.TabStop = false;
            this.neighbourhoodGroupBox.Text = "Neighbourhood";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(68, 66);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(55, 13);
            this.label9.TabIndex = 24;
            this.label9.Text = "Probability";
            // 
            // mooreNumeric
            // 
            this.mooreNumeric.Location = new System.Drawing.Point(68, 43);
            this.mooreNumeric.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.mooreNumeric.Name = "mooreNumeric";
            this.mooreNumeric.Size = new System.Drawing.Size(62, 20);
            this.mooreNumeric.TabIndex = 24;
            this.mooreNumeric.Value = new decimal(new int[] {
            75,
            0,
            0,
            0});
            this.mooreNumeric.ValueChanged += new System.EventHandler(this.mooreNumeric_ValueChanged);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Checked = true;
            this.radioButton2.Location = new System.Drawing.Point(7, 44);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(55, 17);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Moore";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(7, 20);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(92, 17);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.Text = "von Neumann";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.radioButton8);
            this.groupBox2.Controls.Add(this.radioButton7);
            this.groupBox2.Location = new System.Drawing.Point(9, 165);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(156, 74);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Boundry Conditions";
            // 
            // radioButton8
            // 
            this.radioButton8.AutoSize = true;
            this.radioButton8.Checked = true;
            this.radioButton8.Location = new System.Drawing.Point(7, 44);
            this.radioButton8.Name = "radioButton8";
            this.radioButton8.Size = new System.Drawing.Size(63, 17);
            this.radioButton8.TabIndex = 1;
            this.radioButton8.TabStop = true;
            this.radioButton8.Text = "Periodic";
            this.radioButton8.UseVisualStyleBackColor = true;
            this.radioButton8.CheckedChanged += new System.EventHandler(this.radioButton8_CheckedChanged);
            // 
            // radioButton7
            // 
            this.radioButton7.AutoSize = true;
            this.radioButton7.Location = new System.Drawing.Point(8, 20);
            this.radioButton7.Name = "radioButton7";
            this.radioButton7.Size = new System.Drawing.Size(82, 17);
            this.radioButton7.TabIndex = 0;
            this.radioButton7.Text = "Nonperiodic";
            this.radioButton7.UseVisualStyleBackColor = true;
            this.radioButton7.CheckedChanged += new System.EventHandler(this.radioButton7_CheckedChanged);
            // 
            // startButton
            // 
            this.startButton.Location = new System.Drawing.Point(9, 6);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(75, 23);
            this.startButton.TabIndex = 3;
            this.startButton.Text = "Start";
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Click += new System.EventHandler(this.startButton_Click);
            // 
            // stopButton
            // 
            this.stopButton.Enabled = false;
            this.stopButton.Location = new System.Drawing.Point(90, 6);
            this.stopButton.Name = "stopButton";
            this.stopButton.Size = new System.Drawing.Size(75, 23);
            this.stopButton.TabIndex = 4;
            this.stopButton.Text = "Stop";
            this.stopButton.UseVisualStyleBackColor = true;
            this.stopButton.Click += new System.EventHandler(this.stopButton_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(90, 36);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = "Step";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // clearButton
            // 
            this.clearButton.Location = new System.Drawing.Point(9, 36);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(75, 23);
            this.clearButton.TabIndex = 8;
            this.clearButton.Text = "Clear";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.firstTabPage);
            this.tabControl1.Controls.Add(this.thridTabPage);
            this.tabControl1.Location = new System.Drawing.Point(618, 33);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(200, 282);
            this.tabControl1.TabIndex = 22;
            // 
            // energyBox
            // 
            this.energyBox.Location = new System.Drawing.Point(12, 415);
            this.energyBox.Name = "energyBox";
            this.energyBox.Size = new System.Drawing.Size(200, 100);
            this.energyBox.TabIndex = 23;
            this.energyBox.TabStop = false;
            // 
            // energyGroupBox
            // 
            this.energyGroupBox.Controls.Add(this.label11);
            this.energyGroupBox.Controls.Add(this.highEnergyNumericUpDown3);
            this.energyGroupBox.Controls.Add(this.label10);
            this.energyGroupBox.Controls.Add(this.lowEnergyNumericUpDown);
            this.energyGroupBox.Controls.Add(this.heterogenRadio);
            this.energyGroupBox.Controls.Add(this.homogenRadio);
            this.energyGroupBox.Location = new System.Drawing.Point(219, 415);
            this.energyGroupBox.Name = "energyGroupBox";
            this.energyGroupBox.Size = new System.Drawing.Size(125, 100);
            this.energyGroupBox.TabIndex = 24;
            this.energyGroupBox.TabStop = false;
            this.energyGroupBox.Text = "Energy Distribution";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Enabled = false;
            this.label11.Location = new System.Drawing.Point(56, 82);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(64, 13);
            this.label11.TabIndex = 27;
            this.label11.Text = "High energy";
            this.label11.Visible = false;
            // 
            // highEnergyNumericUpDown3
            // 
            this.highEnergyNumericUpDown3.Enabled = false;
            this.highEnergyNumericUpDown3.Location = new System.Drawing.Point(7, 80);
            this.highEnergyNumericUpDown3.Maximum = new decimal(new int[] {
            31,
            0,
            0,
            0});
            this.highEnergyNumericUpDown3.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.highEnergyNumericUpDown3.Name = "highEnergyNumericUpDown3";
            this.highEnergyNumericUpDown3.Size = new System.Drawing.Size(43, 20);
            this.highEnergyNumericUpDown3.TabIndex = 26;
            this.highEnergyNumericUpDown3.Value = new decimal(new int[] {
            7,
            0,
            0,
            0});
            this.highEnergyNumericUpDown3.Visible = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(56, 60);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(62, 13);
            this.label10.TabIndex = 25;
            this.label10.Text = "Low energy";
            // 
            // lowEnergyNumericUpDown
            // 
            this.lowEnergyNumericUpDown.Location = new System.Drawing.Point(7, 58);
            this.lowEnergyNumericUpDown.Maximum = new decimal(new int[] {
            31,
            0,
            0,
            0});
            this.lowEnergyNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.lowEnergyNumericUpDown.Name = "lowEnergyNumericUpDown";
            this.lowEnergyNumericUpDown.Size = new System.Drawing.Size(43, 20);
            this.lowEnergyNumericUpDown.TabIndex = 2;
            this.lowEnergyNumericUpDown.Value = new decimal(new int[] {
            6,
            0,
            0,
            0});
            // 
            // heterogenRadio
            // 
            this.heterogenRadio.AutoSize = true;
            this.heterogenRadio.Location = new System.Drawing.Point(7, 35);
            this.heterogenRadio.Name = "heterogenRadio";
            this.heterogenRadio.Size = new System.Drawing.Size(92, 17);
            this.heterogenRadio.TabIndex = 1;
            this.heterogenRadio.Text = "Heterogenous";
            this.heterogenRadio.UseVisualStyleBackColor = true;
            this.heterogenRadio.CheckedChanged += new System.EventHandler(this.heterogenRadio_CheckedChanged);
            // 
            // homogenRadio
            // 
            this.homogenRadio.AutoSize = true;
            this.homogenRadio.Checked = true;
            this.homogenRadio.Location = new System.Drawing.Point(7, 14);
            this.homogenRadio.Name = "homogenRadio";
            this.homogenRadio.Size = new System.Drawing.Size(88, 17);
            this.homogenRadio.TabIndex = 0;
            this.homogenRadio.TabStop = true;
            this.homogenRadio.Text = "Homogenous";
            this.homogenRadio.UseVisualStyleBackColor = true;
            this.homogenRadio.CheckedChanged += new System.EventHandler(this.homogenRadio_CheckedChanged);
            // 
            // distributeEnergyButton
            // 
            this.distributeEnergyButton.Location = new System.Drawing.Point(350, 423);
            this.distributeEnergyButton.Name = "distributeEnergyButton";
            this.distributeEnergyButton.Size = new System.Drawing.Size(75, 23);
            this.distributeEnergyButton.TabIndex = 25;
            this.distributeEnergyButton.Text = "Distribute";
            this.distributeEnergyButton.UseVisualStyleBackColor = true;
            this.distributeEnergyButton.Click += new System.EventHandler(this.distributeEnergyButton_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.incrementalNucleationTextBox);
            this.groupBox5.Controls.Add(this.constanNucleationTextBox);
            this.groupBox5.Controls.Add(this.incrementalNucleationRadio);
            this.groupBox5.Controls.Add(this.constantNucleationRadio);
            this.groupBox5.Location = new System.Drawing.Point(440, 415);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(138, 73);
            this.groupBox5.TabIndex = 26;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Nucleation";
            // 
            // constantNucleationRadio
            // 
            this.constantNucleationRadio.AutoSize = true;
            this.constantNucleationRadio.Checked = true;
            this.constantNucleationRadio.Location = new System.Drawing.Point(17, 19);
            this.constantNucleationRadio.Name = "constantNucleationRadio";
            this.constantNucleationRadio.Size = new System.Drawing.Size(67, 17);
            this.constantNucleationRadio.TabIndex = 0;
            this.constantNucleationRadio.TabStop = true;
            this.constantNucleationRadio.Text = "Constant";
            this.constantNucleationRadio.UseVisualStyleBackColor = true;
            this.constantNucleationRadio.CheckedChanged += new System.EventHandler(this.constantNucleationRadio_CheckedChanged);
            // 
            // incrementalNucleationRadio
            // 
            this.incrementalNucleationRadio.AutoSize = true;
            this.incrementalNucleationRadio.Location = new System.Drawing.Point(17, 45);
            this.incrementalNucleationRadio.Name = "incrementalNucleationRadio";
            this.incrementalNucleationRadio.Size = new System.Drawing.Size(80, 17);
            this.incrementalNucleationRadio.TabIndex = 1;
            this.incrementalNucleationRadio.Text = "Incremental";
            this.incrementalNucleationRadio.UseVisualStyleBackColor = true;
            this.incrementalNucleationRadio.CheckedChanged += new System.EventHandler(this.incrementalNucleationRadio_CheckedChanged);
            // 
            // constanNucleationTextBox
            // 
            this.constanNucleationTextBox.Location = new System.Drawing.Point(101, 19);
            this.constanNucleationTextBox.Name = "constanNucleationTextBox";
            this.constanNucleationTextBox.Size = new System.Drawing.Size(28, 20);
            this.constanNucleationTextBox.TabIndex = 2;
            // 
            // incrementalNucleationTextBox
            // 
            this.incrementalNucleationTextBox.Enabled = false;
            this.incrementalNucleationTextBox.Location = new System.Drawing.Point(101, 43);
            this.incrementalNucleationTextBox.Name = "incrementalNucleationTextBox";
            this.incrementalNucleationTextBox.Size = new System.Drawing.Size(28, 20);
            this.incrementalNucleationTextBox.TabIndex = 3;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.bordersOnlyRadio);
            this.groupBox6.Controls.Add(this.unconstrainedRadio);
            this.groupBox6.Location = new System.Drawing.Point(587, 415);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(106, 73);
            this.groupBox6.TabIndex = 27;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Nucleons location";
            // 
            // bordersOnlyRadio
            // 
            this.bordersOnlyRadio.AutoSize = true;
            this.bordersOnlyRadio.Location = new System.Drawing.Point(6, 41);
            this.bordersOnlyRadio.Name = "bordersOnlyRadio";
            this.bordersOnlyRadio.Size = new System.Drawing.Size(84, 17);
            this.bordersOnlyRadio.TabIndex = 3;
            this.bordersOnlyRadio.Text = "Only borders";
            this.bordersOnlyRadio.UseVisualStyleBackColor = true;
            // 
            // unconstrainedRadio
            // 
            this.unconstrainedRadio.AutoSize = true;
            this.unconstrainedRadio.Checked = true;
            this.unconstrainedRadio.Location = new System.Drawing.Point(6, 15);
            this.unconstrainedRadio.Name = "unconstrainedRadio";
            this.unconstrainedRadio.Size = new System.Drawing.Size(94, 17);
            this.unconstrainedRadio.TabIndex = 2;
            this.unconstrainedRadio.TabStop = true;
            this.unconstrainedRadio.Text = "Unconstrained";
            this.unconstrainedRadio.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(837, 541);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.distributeEnergyButton);
            this.Controls.Add(this.energyGroupBox);
            this.Controls.Add(this.energyBox);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.iterationCounterLabel);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.numericMonteCarlo);
            this.Controls.Add(this.mcButton);
            this.Controls.Add(this.recryStopButton);
            this.Controls.Add(this.kNumeric);
            this.Controls.Add(this.recrystalizeButton);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.pictureBox);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Crystalization";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.colourBox)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radiusNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.evenNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grainRatioNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMonteCarlo)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.inclusionsNumeric)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.thridTabPage.ResumeLayout(false);
            this.selectGroupBox.ResumeLayout(false);
            this.selectGroupBox.PerformLayout();
            this.firstTabPage.ResumeLayout(false);
            this.neighbourhoodGroupBox.ResumeLayout(false);
            this.neighbourhoodGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mooreNumeric)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.energyBox)).EndInit();
            this.energyGroupBox.ResumeLayout(false);
            this.energyGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.highEnergyNumericUpDown3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lowEnergyNumericUpDown)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox colourBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.NumericUpDown grainRatioNumeric;
        private System.Windows.Forms.Button randomButton;
        private System.Windows.Forms.NumericUpDown evenNumeric;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.NumericUpDown radiusNumeric;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button radiusButton;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown numericUpDown2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Timer recrystalizationTimer;
        private System.Windows.Forms.Button recrystalizeButton;
        private System.Windows.Forms.NumericUpDown kNumeric;
        private System.Windows.Forms.Button recryStopButton;
        private System.Windows.Forms.NumericUpDown numericMonteCarlo;
        private System.Windows.Forms.Button mcButton;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Timer monteCarloTimer;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label iterationCounterLabel;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem imageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bitmapToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem textToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importFromToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem imageToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem textToolStripMenuItem1;
        private System.Windows.Forms.NumericUpDown inclusionsNumeric;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton radialInclusionRadioButton;
        private System.Windows.Forms.RadioButton squareInclusionRadioButton;
        private System.Windows.Forms.TabPage thridTabPage;
        private System.Windows.Forms.Button selectGrainButton;
        private System.Windows.Forms.ListBox grainsListBox;
        private System.Windows.Forms.TabPage firstTabPage;
        private System.Windows.Forms.GroupBox neighbourhoodGroupBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown mooreNumeric;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton radioButton8;
        private System.Windows.Forms.RadioButton radioButton7;
        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.Button stopButton;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button clearButton;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.GroupBox selectGroupBox;
        private System.Windows.Forms.CheckBox randomizeCheckBox;
        private System.Windows.Forms.CheckBox dualPhaseCheckBox;
        private System.Windows.Forms.Button detectEdgesButton;
        private System.Windows.Forms.CheckBox mcRandomizeCheckbox;
        private System.Windows.Forms.PictureBox energyBox;
        private System.Windows.Forms.GroupBox energyGroupBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.NumericUpDown highEnergyNumericUpDown3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown lowEnergyNumericUpDown;
        private System.Windows.Forms.RadioButton heterogenRadio;
        private System.Windows.Forms.RadioButton homogenRadio;
        private System.Windows.Forms.Button distributeEnergyButton;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RadioButton incrementalNucleationRadio;
        private System.Windows.Forms.RadioButton constantNucleationRadio;
        private System.Windows.Forms.TextBox incrementalNucleationTextBox;
        private System.Windows.Forms.TextBox constanNucleationTextBox;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.RadioButton bordersOnlyRadio;
        private System.Windows.Forms.RadioButton unconstrainedRadio;
    }
}

