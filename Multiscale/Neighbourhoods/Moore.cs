﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using Multiscale.Neighbourhoods;

namespace Multiscale
{
    class MooreNeighbourhood : Neighbourhood
    {
        private static readonly Offset[] neighbourOffsets = Offset.offsets;
        private VonNeumannNeighbourhood neumann;
        private FurtherMooreNeighbourhood furtherMoore;
        private Random rand = new Random();
        private int chance;
        public MooreNeighbourhood(IIndexTranslator idx, int chance = 1) : base(idx)
        {
            if (chance < 1 || chance > 100)
                throw new ArgumentException();
            neumann = new VonNeumannNeighbourhood(idx);
            furtherMoore = new FurtherMooreNeighbourhood(idx);
            this.chance = chance;
        }

        protected internal override IEnumerable<Tuple<short,int>> CalculateGrainCounters(short[,] board, Point point, List<short> ignoredGrainIds)
        {
            var x = point.X; var y = point.Y;
            var n = new List<short>(8)
            {
                board[Idx.X(x - 1), Idx.Y(y)],
                board[Idx.X(x + 1), Idx.Y(y)]
            };
            for (var i = -1; i <= 1; ++i)
                n.Add(board[Idx.X(x + i), Idx.Y(y - 1)]);
            for (var i = -1; i <= 1; ++i)
                n.Add(board[Idx.X(x + i), Idx.Y(y + 1)]);

            var counters = n.Where(o => o > 0)
                .Distinct()
                .Select(l => new Tuple<short, int>(l, n.Count(num => num.Equals(l))));
            var moorCountersCache = counters.ToList();
            //Rule 1
            if (moorCountersCache.Count > 0 && moorCountersCache.First().Item2 > 4)
                return new[] { moorCountersCache.First() };
            //Rule 2
            counters = neumann.CalculateGrainCounters(board, point, ignoredGrainIds).OrderByDescending(el => el.Item2);
            if (counters.Count() > 0 && counters.First().Item2 >= 3)
                return new[] { counters.First() };
            //Rule 3
            counters = furtherMoore.CalculateGrainCounters(board, point, ignoredGrainIds).OrderByDescending(el => el.Item2);
            if (counters.Count() > 0 && counters.First().Item2 >= 3)
                return new[] { counters.First() };
            //Rule 4
            var randInt = rand.Next(101); 
            if (randInt < chance && moorCountersCache.Count > 0)
                return new[] { moorCountersCache.First() };
            else
                return Enumerable.Empty<Tuple<short, int>>();
        }

        public bool IsOneTheEdge(short[,] board, Point point)
        {
            var x = point.X; var y = point.Y;
            var n = new List<short>(8)
            {
                board[Idx.X(x - 1), Idx.Y(y)],
                board[Idx.X(x + 1), Idx.Y(y)]
            };
            for (var i = -1; i <= 1; ++i)
                n.Add(board[Idx.X(x + i), Idx.Y(y - 1)]);
            for (var i = -1; i <= 1; ++i)
                n.Add(board[Idx.X(x + i), Idx.Y(y + 1)]);
            var grainId = board[Idx.X(x), Idx.Y(y)];
            var differentCellsCount = n.Distinct().Where(o => o >= 0 && o != grainId).Count();

            bool isEmptySpace = grainId == 0 && differentCellsCount == 0;
            if (isEmptySpace)
                return true;

            return differentCellsCount > 0;
        }

        public override int CountDifferentGrains(short[,] board, short grainType, Point point)
        {
            var differentGrainsCount = 0;
            var x = point.X; var y = point.Y;

            if (board[Idx.X(x - 1), Idx.Y(y)] != grainType) ++differentGrainsCount;
            if (board[Idx.X(x + 1), Idx.Y(y)] != grainType) ++differentGrainsCount;
            for (var i = -1; i <= 1; ++i)
                if (board[Idx.X(x + i), Idx.Y(y - 1)] != grainType) ++differentGrainsCount;
            for (var i = -1; i <= 1; ++i)
                if (board[Idx.X(x + i), Idx.Y(y + 1)] != grainType) ++differentGrainsCount;

            return differentGrainsCount;
        }

        public override short RandomNeighbour(short[,] board, Point point, Random random)
        {
            var randomIndex = random.Next(0, neighbourOffsets.Length);
            var randomOffset = neighbourOffsets[randomIndex];
            return board[
                Idx.X(randomOffset.X + point.X),
                Idx.Y(randomOffset.Y + point.Y)
                ];
        }

        public override int NeighboursCount()
        {
            return 8;
        }

        public bool TryGetRandomRecrystalizedNeighbour(short[,] board, bool[,] isRecrystalized, Point point, Random random, out short recrystalizedNeighbour)
        {
            recrystalizedNeighbour = -128;
            bool anyRecrystalized = neighbourOffsets.Select(off => isRecrystalized[Idx.X(point.X + off.X), Idx.Y(point.Y + off.Y)])
                .Any(flag => flag == true);

            if (anyRecrystalized == false)
                return anyRecrystalized;

            var recrystalizedGrains = neighbourOffsets.Where(off => isRecrystalized[Idx.X(point.X + off.X), Idx.Y(point.Y + off.Y)])
                .Select(off => board[Idx.X(point.X + off.X), Idx.Y(point.Y + off.Y)])
                .Distinct()
                .ToArray();
            recrystalizedNeighbour = recrystalizedGrains[random.Next(0, recrystalizedGrains.Length)];
            return true;
        }
    }
}
