﻿using Multiscale.Neighbourhoods;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Multiscale
{
    class VonNeumannNeighbourhood : Neighbourhood
    {
        public static readonly Offset[] neighbourOffsets = { Offset.offsets[1], Offset.offsets[3], Offset.offsets[4], Offset.offsets[6] };
        public VonNeumannNeighbourhood(IIndexTranslator idx) : base(idx)
        {}

        public override int CountDifferentGrains(short[,] board, short grainType, Point point)
        {
            var differentGrainsCount = 0;
            var x = point.X;
            var y = point.Y;

            if (board[Idx.X(x - 1), Idx.Y(y)] != grainType) ++differentGrainsCount;
            if (board[Idx.X(x + 1), Idx.Y(y)] != grainType) ++differentGrainsCount;
            if (board[Idx.X(x), Idx.Y(y - 1)] != grainType) ++differentGrainsCount;
            if (board[Idx.X(x), Idx.Y(y + 1)] != grainType) ++differentGrainsCount;

            return differentGrainsCount;
        }

        public override int NeighboursCount()
        {
            return 4;
        }

        public override short RandomNeighbour(short[,] board, Point point, Random random)
        {
            var randomIndex = random.Next(0, neighbourOffsets.Length - 1);
            var randomOffset = neighbourOffsets[randomIndex];
            return board[
                Idx.X(randomOffset.X + point.X),
                Idx.Y(randomOffset.Y + point.Y)
                ];
        }

        protected internal override IEnumerable<Tuple<short, int>> CalculateGrainCounters(short[,] board, Point point, List<short> ignoredGrainIds)
        {
            var x = point.X; var y = point.Y;
            var crystals = new List<short> { board[Idx.X(x - 1), Idx.Y(y)],
                board[Idx.X(x + 1), Idx.Y(y)],
                board[Idx.X(x), Idx.Y(y - 1)],
                board[Idx.X(x), Idx.Y(y + 1)] };

            return crystals
                .Where(o => o > 0)
                .Distinct()
                .Select(l => new Tuple<short, int>(l, crystals.Count(num => num.Equals(l))));
        }

    }
}
