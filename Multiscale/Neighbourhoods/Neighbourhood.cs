﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;

namespace Multiscale
{
    abstract class Neighbourhood
    {
        public IIndexTranslator Idx { get; set; }

        protected internal abstract IEnumerable<Tuple<short, int>> CalculateGrainCounters(short[,] board, Point point, List<short> ignoredGrainIds);

        public abstract short RandomNeighbour(short[,] board, Point point, Random random);

        public abstract int CountDifferentGrains(short[,] board, short grainType, Point point);

        public virtual int GetCrystalizationResult(short[,] board, Point point, List<short> ignoredGrainIds)
        {
            var a = CalculateGrainCounters(board, point, ignoredGrainIds)
                .Where(tup => !ignoredGrainIds.Contains(tup.Item1));
            if (a.Count() == 0) return 0;

            var max = a.Max(o => o.Item2);
            return a.Where(o => o.Item2 == max).First().Item1;  
        }

        public abstract int NeighboursCount();

        public Neighbourhood(IIndexTranslator idx)
        {
            Idx = idx;
        }
    }
}
