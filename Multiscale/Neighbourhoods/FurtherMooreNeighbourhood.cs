﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Multiscale.Neighbourhoods
{
    class FurtherMooreNeighbourhood : Neighbourhood
    {
        public FurtherMooreNeighbourhood(IIndexTranslator idx) : base(idx)
        {
        }

        public override int CountDifferentGrains(short[,] board, short grainType, Point point)
        {
            throw new NotImplementedException();
        }

        public override int NeighboursCount()
        {
            return 4;
        }

        public override short RandomNeighbour(short[,] board, Point point, Random random)
        {
            throw new NotImplementedException();
        }

        protected internal override IEnumerable<Tuple<short, int>> CalculateGrainCounters(short[,] board, Point point, List<short> ignoredGrainIds)
        {
            var x = point.X; var y = point.Y;
            var n = new List<short>(8)
            {
                board[Idx.X(x - 1), Idx.Y(y - 1)],
                board[Idx.X(x + 1), Idx.Y(y - 1)],
                board[Idx.X(x - 1), Idx.Y(y + 1)],
                board[Idx.X(x + 1), Idx.Y(y + 1)]
            };
            return n.Where(o => o > 0)
                .Distinct()
                .Select(l => new Tuple<short, int>(l, n.Count(num => num.Equals(l))));
        }
    }
}
