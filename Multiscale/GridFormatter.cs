﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Multiscale
{
    class GridFormatter
    {
        const int pixelsPerCell = 3;
        public Bitmap ToBitmap(Grid grid, List<Color> colours)
        {
            const int boarderSize = 2;
            var bitmap = new Bitmap(
                (grid.board.GetLength(0) - boarderSize) * pixelsPerCell,
                (grid.board.GetLength(1) - boarderSize) * pixelsPerCell
                );

            for(var x = 1; x< grid.board.GetLength(0)- 1; ++x)
                for(var y = 1; y < grid.board.GetLength(1) - 1; ++y)
                {
                    var currCell = grid.board[x, y];
                    var currColour = Color.White;
                    if (currCell == -1)
                        currColour = Color.Black;
                    else if (currCell != 0)
                        currColour = colours[currCell - 1];
                    
                    int xPix = (x - 1) * pixelsPerCell,
                        yPix = (y - 1) * pixelsPerCell;

                    for (var i = 0; i < pixelsPerCell; ++i)
                        for (var j = 0; j < pixelsPerCell; ++j)
                            bitmap.SetPixel(xPix + i, yPix + j, currColour);
                }

            return bitmap;
        }

        public StringBuilder ToStringBuilder(Grid grid, List<Color> colours)
        {
            var strBuilder = new StringBuilder();

            strBuilder.AppendLine(colours.Count.ToString());
            foreach (var colour in colours)
            {
                strBuilder.AppendLine($"{colour.R};{colour.G};{colour.B}");
            }

            const int boarderSize = 2;
            strBuilder.AppendLine($"{grid.board.GetLength(0) - boarderSize} {grid.board.GetLength(1) - boarderSize}");
            for (var x = 1; x < grid.board.GetLength(0) - 1; ++x)
                for (var y = 1; y < grid.board.GetLength(1) - 1; ++y)
                {
                    strBuilder.AppendLine($"{x} {y} {grid.board[x, y]}");
                }

            return strBuilder;
        }

        public Tuple<Grid, List<Color>> FromText(string fileContents)
        {
            var lines = fileContents.Split( new[] { Environment.NewLine },
                StringSplitOptions.None
                );

            var currLine = 0;
            var coloursCount = int.Parse(lines[currLine++]);
            var colours = new List<Color>(coloursCount);
            for (var i = 0; i< coloursCount; ++i)
            {
                var RGB = lines[currLine++].Split(new[] { ';' })
                    .Select( x => int.Parse(x)).ToArray();
                colours.Add(Color.FromArgb(RGB[0], RGB[1], RGB[2]));
            }


            var dimensions = lines[currLine++].Split(new[] { ' ' },
                StringSplitOptions.None)
                .Select(x => int.Parse(x))
                .ToArray();
            var grid = new Grid(dimensions[0], dimensions[1]);
            var cellsCount = dimensions[0] * dimensions[1];
            for (int i = 0; i < cellsCount; ++i)
            {
                var cell = lines[currLine++].Split(new[] { ' ' })
                    .Select(num => int.Parse(num))
                    .ToArray();
                int x = cell[0],
                    y = cell[1];
                short grainId = (short)cell[2];

                grid.board[x, y] = grainId;
            }

            return new Tuple<Grid, List<Color>>(grid, colours);
        }

        public Tuple<Grid, List<Color>> FromImage(Bitmap bitmap)
        {
            var colours = new List<Color>();
            for (int i = 0; i < bitmap.Width; i += pixelsPerCell)
                for (int j = 0; j < bitmap.Height; j += pixelsPerCell)
                    colours.Add(bitmap.GetPixel(i, j));
            colours = colours.Distinct()
                .Where(colour => colour.ToArgb() != Color.Black.ToArgb() 
                                && colour.ToArgb() != Color.White.ToArgb())
                .ToList();

            var grid = new Grid(
                bitmap.Width / pixelsPerCell,
                bitmap.Height / pixelsPerCell
                );
            for (int x = 0; x < bitmap.Width; x += pixelsPerCell)
            {
                for (int y = 0; y < bitmap.Height; y += pixelsPerCell)
                {
                    int cellX = x / pixelsPerCell + 1,
                        cellY = y / pixelsPerCell + 1;
                    var pixelColour = bitmap.GetPixel(x, y);
                    short grainId = 0;
                    if (pixelColour.ToArgb() == Color.Black.ToArgb())
                        grainId = -1;
                    else if (pixelColour.ToArgb() == Color.White.ToArgb())
                        grainId = 0;
                    else
                        grainId = (short)(colours.IndexOf(
                            bitmap.GetPixel(x, y)
                            ) + 1);

                    grid.board[cellX, cellY] = grainId;
                }
            }

            return new Tuple<Grid, List<Color>>(grid, colours);
        }
    }
}
