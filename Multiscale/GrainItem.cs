﻿using System.Drawing;

namespace Multiscale
{
    class GrainItem
    {
        public string Text;
        public Color Color;
        public short GrainId;
        public int Count;
    }
}
