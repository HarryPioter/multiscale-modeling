﻿namespace Multiscale
{
    public class PeriodicIndexTranslator : IIndexTranslator
    {
        private int SizeX { get; set; }
        private int SizeY { get; set; }

        public PeriodicIndexTranslator(int x, int y)
        {
            SizeX = x;
            SizeY = y;
        }

        public int X(int x)
        {
            if (x < 1)
                return SizeX - 2;
            if (x > SizeX - 2)
                return 1;
            return x;
        }

        public int Y(int y)
        {
            if (y < 1)
                return SizeY - 2;
            if (y > SizeY - 2)
                return 1;
            return y;
        }
    }
}
