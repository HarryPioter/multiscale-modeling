﻿namespace Multiscale
{
    class NonperiodicIndexTranslator : IIndexTranslator
    {
        public int X(int x)
        {
            return x;
        }

        public int Y(int y)
        {
            return y;
        }
    }
}
