﻿namespace Multiscale
{
    interface IIndexTranslator
    {
        int X(int x);
        int Y(int y);
    }
}
